# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ChkinAPK',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('version', models.CharField(max_length=45)),
                ('uploaded', models.DateTimeField()),
                ('apk_url', models.CharField(max_length=256, blank=True)),
                ('dump_url', models.CharField(max_length=256, blank=True)),
                ('mapping_url', models.CharField(max_length=256, blank=True)),
                ('seeds_url', models.CharField(max_length=256, blank=True)),
                ('usage_url', models.CharField(max_length=256, blank=True)),
                ('build_id', models.CharField(max_length=45)),
            ],
        ),
    ]
