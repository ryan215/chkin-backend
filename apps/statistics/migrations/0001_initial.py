# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmsEmailNotificationLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('alert_type', models.CharField(max_length=45)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('camera', models.ForeignKey(to='camera.Camera')),
            ],
        ),
        migrations.CreateModel(
            name='Statistics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('daily', models.IntegerField()),
                ('weekly', models.IntegerField()),
                ('monthly', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Usage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('downloaded', models.ForeignKey(related_name='download_usage', to='statistics.Statistics')),
                ('storage', models.ForeignKey(related_name='storage_usage', to='statistics.Statistics')),
            ],
        ),
    ]
