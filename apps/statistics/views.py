import json
from logging import debug as d, info as i, warning as w, error as e, critical as c
from django.conf import settings
from django.core.mail import EmailMessage
from django.views.generic import View
from apps.camera.models import Camera
from apps.camera.utils import EmailSmsViewMixin
from chkincam.utils.exceptions import ErrorConvention
from chkincam.utils.views import CameraAuthView, ChkincamResponseMixin


class SendEmailSmsAlert(CameraAuthView, EmailSmsViewMixin):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        camera_id = request.REQUEST.get('camera_id')
        if camera_id is not None:
            camera = Camera.objects.get(pk=camera_id)
        else:
            camera = self.auth
            if not isinstance(camera, Camera):
                return self.error("NOT APPLICABLE")

        json_state = request.REQUEST.get('json_state')
        if json_state:
            kwargs = json.loads(json_state)
            e("email alert using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                return self.error("MALFORMED_JSON")
        else:
            e("email alert using kwargs: {0}".format(repr(kwargs)))

        for name, value in kwargs.items():
            e("_email_alert({0}, {1})".format(repr(name), repr(value)))

            if value == "true":
                state = camera.params_to_dict()
                camera_name = ""

                if 'device_title' in state and state['device_title']:
                    camera_name = state["device_title"]

                mac_address = camera.name

                if camera.owner is None:
                    raise ErrorConvention("NOT APPLICABLE")

                cuser = camera.owner
                receiver = cuser.name
                phone_number = cuser.phone_number
                telephone_co = cuser.telephone_co
                timezone_str = ""

                if 'camera_timezone_local_area' in state and state['camera_timezone_local_area']:
                    timezone_str = state["camera_timezone_local_area"]

                self._send_email_sms_alert(name, receiver, phone_number, telephone_co, camera_name, mac_address,
                                           camera, timezone_str)

            e("send email alert after set: {0}".format(repr(name)))

        return self.ok()

class TestMail(ChkincamResponseMixin, EmailSmsViewMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        self._send_email_sms_alert("offline_alert_email", "seaman9297@gmail.com", "13480862321", "sfsd32223", "dog",
                                   "ddd232121", Camera.objects.first(), "America/Los_Angeles")
        return self.ok()


class TestSms(ChkincamResponseMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        json_state = request.GET.get('json_state')

        if json_state:
            kwargs = json.loads(json_state)
            e("account infos using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                raise ErrorConvention("MALFORMED_JSON")
        else:
            e("account infos using kwargs: {0}".format(repr(kwargs)))

        phone_number = ""
        telephone_co = ""
        for name, value in kwargs.items():
            e("_set_account_infos(camera, {0}, {1})".format(repr(name), repr(value)))
            if name == "phone_number":
                phone_number = value
            if name == "telephone_co":
                telephone_co = value

        subject = "Test SMS"
        body = "This Message is sent for test SMS."
        new_receiver = phone_number + telephone_co
        w(phone_number)
        w(telephone_co)
        w(new_receiver)
        email = EmailMessage(subject, body, settings.SERVER_SENDER_ADDRESS,
                             [new_receiver], cc=['seaman9297@gmail.com'])

        email.send()

        return self.ok()
