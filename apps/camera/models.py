from datetime import datetime, timedelta
from django.conf import settings
from django.db import models
# from apps.android.models import GCMCommand
# from apps.android.utils import GCMMessage, GCMConnection
from apps.statistics.models import Usage
from logging import debug as d, info as i, warning as w, error as e, critical as c
from chkincam.utils import date_to_timestamp
from chkincam.utils import key_to_jsonparam, key_from_jsonparam

import paho.mqtt.publish as publish

class Camera(models.Model):
	CAMERA_DEFAULT_VALUES = {
		"recording_quality": "HD",
		"night_vision": "false",
		"motion_sensor": "false",
		"microphone": "true",
		"speaker": "false",
		"external_ip": "-1",
		"internal_ip": "-1",
		"external_port": "-1",
		"internal_port": "-1",
		"recording_mode": "false",
		"manual_port": "-1",
		"email_alert_active": "false",
		"motion_event_alert_email": "false",
		"motion_event_alert_push": "false",
		"loud_noise_alert_email": "false",
		"loud_noise_alert_push": "false",
		"camera_offline_alert_push": "false",
		"camera_offline_alert_email": "false",
		"schedule_active": "false",
		"schedule_by_days": "None",
		"schedule_from": "None",
		"schedule_to": "None",
		"subscription": "false",
		"push_token": "",
		"device_title": "",
		"camera_timezone_time_diff": "0",
		"wakeup_auto_monitor": "false",
		"last_hartbeat_at": "-1",
		"online": "false",
		"chkin_version": "1.0.0",
		"camera_timezone_local_area": "",
		"camera_offline_alert": "false",
		"motion_event_alert": "false",
		"loud_noise_alert": "false",
		"camera_timezone": "",
		"camera_timezone_string": "",
		"push_alert_active": "false",
		"email_alert": "false",
		"device_address": "",
		"gcm_push_token": ""
	}

	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='camera_owner', blank=True, null=True)
	claim_token = models.CharField(max_length=128, blank=True, null=True)
	build_id = models.CharField(max_length=45)
	# used for storing camera's mac address
	name = models.CharField(max_length=45)
	user = models.CharField(max_length=45, blank=True, null=True)
	deleted = models.BooleanField(default=False)
	usage = models.ForeignKey(Usage, related_name='camera', blank=True, null=True)
	creation_date = models.DateTimeField(auto_now=True)
	device_title = models.CharField(max_length=100, blank=True)

	challenge = models.CharField(max_length=45, blank=True)
	session_key = models.CharField(max_length=100, blank=True)
	session_created = models.DateTimeField(blank=True, null=True)
	password = models.CharField(max_length=100, blank=True)

	def params_to_dict(self):
		ret = {}

		for param in self.parameter_values.all():
			param_name = param.name
			if not (param_name in self.CAMERA_DEFAULT_VALUES.keys()):
				e(
					"params_to_dict>>>> " + param_name + " not defined as a default camera value, skipping this field !!!")
				continue
			ret[param_name] = param.value

		return ret

	def segments(self, from_time, to_time):
		if from_time is None:
			from_time = datetime(1970, 1, 1)
		else:
			from_time = datetime.fromtimestamp(float(from_time))
		if to_time is None:
			to_time = datetime.now()
		else:
			to_time = datetime.fromtimestamp(float(to_time))
		segments = self._all_segments_(from_time, to_time)
		return segments

	def _all_segments_(self, from_time, to_time):
		segments = []
		videos = VideoSegment.objects.filter(owner=self, utc_start__gt=from_time,
											 utc_start__lt=to_time, deleted=False)
		for video_segment in videos:
			# do not send videos that has 0 length.
			# fixing corrupted video segments by checking size,
			# from the sample data, vidoes lengths start from 60s. and not lesser than 1 MB.
			# if video_segment.duration != 0.0 and video_segment.size > 1000000:
			## TODO replace video_url with actual network URL
			client_id = key_to_jsonparam(self.name)
			video_url = "";
			if not (video_segment.video_url == None  or video_segment.video_url == ""):
				video_url = "http://{host}:{port}/pull_data/{relative_path}?video_name={video_name}".format(
						host=settings.SERVER_HOSTNAME,
						port=settings.SERVER_PORT,
						relative_path=video_segment.video_url,
						video_name=video_segment.video_name
				)
			thumbnail_url = ""
			if not (video_segment.thumbnail_url == None or video_segment.thumbnail_url == ""):
				thumbnail_url = "http://{host}:{port}/pull_data/{relative_path}".format(
						host=settings.SERVER_HOSTNAME,
						port=settings.SERVER_PORT,
						client_id=client_id,
						relative_path=video_segment.thumbnail_url
				)

			if (video_url == "" and thumbnail_url == "" ):
				continue;
			segments.append({  # "utc_start" : video_segment.utc_start,
					"utc_start": date_to_timestamp(video_segment.utc_start),
					"duration": video_segment.duration,
					"chkin_version": video_segment.chkin_version,
					"video_width": video_segment.video_width,
					"video_height": video_segment.video_height,
					"title": video_segment.title,
					"description": video_segment.description,
					"video_name": video_segment.video_name,
					"video_url": video_url,
					"thumbnail_url": thumbnail_url,
					"thumbnail_width": video_segment.thumbnail_width,
					"thumbnail_height": video_segment.thumbnail_height,
			})
		return segments

	def __unicode__(self):
		return self.name

	def default_camera_state(self):
		# del camera.state[:]
		for key in Camera.CAMERA_DEFAULT_VALUES.keys():
			cam_param = CameraParameter.objects.get_or_create(
				camera=self,
				name=key
			)[0]
			cam_param.value = Camera.CAMERA_DEFAULT_VALUES[key]
			cam_param.save()

	def set_camera_state(self, name, value):
		states = []
		for state in self.parameter_values.all():
			if state.name == name:
				state.value = str(value)
				state.save()
				return
		"Add parameter to camera only if it is a default value"
		if name in Camera.CAMERA_DEFAULT_VALUES.keys():
			states.append(CameraParameter(camera=self, name=name, value=str(value)))
			self.parameter_values = states
		else:
			e(
				"Can not set parameter with name : " + name + " to camera state because it is not defined as camera default value !!!");

	def get_camera_state_value(self, name):
		for state in self.parameter_values.all():
			if state.name == name:
				return state.value

	def get_camera_state(self):
		state = {}
		for param in self.parameter_values.all():
			state[param.name] = param.value
		return state;

	def send_gcm_message(self, payload, collapse_key, expiry_in_seconds=300.0):
		push_token = self.get_camera_state_value("push_token")
		w(" > > > push_token={0}".format(push_token))
		"""
		gcm_message = GCMMessage(self.push_token, payload, collapse_key=collapse_key)
		gcm_conn = GCMConnection()
		gcm_conn.notify_device(gcm_message)
		"""

		userID = "pub_client"
		password = "pub-passwd"
		payload = payload
		auth = {'username':userID, 'password':password}
		i("Sending message to device with id : " + push_token + " , payload : " + payload)
		publish.single(push_token, payload, hostname="67.207.205.6", port=1883, auth=auth)

		command = GCMCommand()
		command.camera = self
		command.utc_expiry = datetime.utcnow() + timedelta(0, expiry_in_seconds)
		command.save()
		return command


class VideoSegment(models.Model):
	owner = models.ForeignKey(Camera, related_name='video_segment_owner')
	utc_start = models.DateTimeField()
	duration = models.FloatField()
	chkin_version = models.CharField(max_length=45)
	video_width = models.CharField(max_length=45)
	video_height = models.CharField(max_length=45)
	title = models.CharField(max_length=45)
	description = models.TextField()
	video_name = models.CharField(max_length=45)
	video_url = models.CharField(max_length=256, blank=True)
	thumbnail_url = models.CharField(max_length=256, blank=True)
	thumbnail_width = models.CharField(max_length=45)
	thumbnail_height = models.CharField(max_length=45)
	camera = models.CharField(max_length=45)
	username = models.CharField(max_length=45)
	# what's the difference between this and owner?
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='video_segment', default=None)
	size = models.IntegerField()
	deleted = models.BooleanField(default=False)

	class Meta:
		ordering = ['-utc_start']


class CameraParameter(models.Model):
	camera = models.ForeignKey(Camera, related_name='parameter_values')
	value = models.CharField(max_length=256, blank=True)
	name = models.CharField(max_length=45)


class GCMCommand(models.Model):
	camera = models.ForeignKey(Camera, related_name='gcm_command')
	utc_expiry = models.DateTimeField()
