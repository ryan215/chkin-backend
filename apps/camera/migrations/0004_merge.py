# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0002_auto_20150617_1244'),
        ('camera', '0003_camera_device_title'),
    ]

    operations = [
    ]
