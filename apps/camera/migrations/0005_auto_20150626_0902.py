# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0004_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videosegment',
            name='user',
            field=models.ForeignKey(related_name='video_segment', default=None, to=settings.AUTH_USER_MODEL),
        ),
    ]
