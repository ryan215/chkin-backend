# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('camera', '0002_auto_20150617_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='camera',
            name='device_title',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
