import os
import base64
import time
import pytz
from json import loads
from logging import info as i, warning as w, error as e
from annoying.functions import get_object_or_None
from datetime import datetime
from validate_email import validate_email
from celery.task.http import URL

from django.contrib.sites.models import Site
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View, CreateView

from apps.camera.models import Camera, VideoSegment
from apps.camera.utils import _authorized_for_asset, _send_blob, EmailSmsViewMixin
from apps.users.models import User
from chkincam.utils import key_to_jsonparam, key_from_jsonparam, date_to_timestamp, generate_session_key, gic
from chkincam.utils.auth import generate_claim_camera_token, generate_challange
from chkincam.utils.views import UserAuthView, ClaimDeviceMixin, CameraAuthView, ChkincamResponseMixin, \
UniversalAuthView
from chkincam.utils.storage import storageManager


class ClaimDeviceRequest(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ClaimDeviceRequest, self).dispatch(request, *args, **kwargs)
		user_email_address = request.REQUEST.get('user_email_address')
		if not validate_email(user_email_address):
			return self.error("BAD EMAIL")
		camera = self.auth
		if not camera:
			return self.error("MISSING LINK")
		if not isinstance(camera, Camera):
			return self.error("NOT APPLICABLE")

		if camera.owner:
			user = camera.owner
			# send the current user email
			subject = "Already claimed"
			body = """
The camera you are trying to claim is already claimed.
"""
			send_mail(subject, body, settings.SERVER_SENDER_ADDRESS, [user_email_address])

			# send the original user
			subject = "Claim warning"
			body = """
Someone just tried to claim your camera.
"""
			send_mail(subject, body, settings.SERVER_SENDER_ADDRESS, [user.email])

		# send email
		camera.claim_token = generate_claim_camera_token()
		camera.save()
		i("claim token generated {0}".format(camera.claim_token))
		confirmation_url = "http://{host}:{port}/claim_device?id={id}&owner={email}".format(
			id=base64.urlsafe_b64encode(camera.claim_token),
			host=settings.SERVER_HOSTNAME,
			port=settings.SERVER_PORT,
			email=base64.urlsafe_b64encode(user_email_address),
		)
		subject = "Confirm camera activation"
		body = """
Thank you for deploying new camera! Please confirm camera activation by
clicking on the link below:

{confirmation_url}
""".format(confirmation_url=confirmation_url)
		mail.send_mail(subject, body, settings.SERVER_SENDER_ADDRESS, [user_email_address])
		w("email body:")
		w(body)
		return self.ok()


class EnterCredentials(ChkincamResponseMixin, ClaimDeviceMixin, View):
	http_method_names = ['post']

	def post(self, request, *args, **kwargs):

		if request.POST.get('password_1') != request.POST.get('password_2'):
			return HttpResponseRedirect(reverse('users:choose_pass'))
		# id = str(cherrypy.request.cookie["id"].value) #?????
		id = str(request.COOKIES.get("id"))
		email = str(request.COOKIES.get("owner")).lower()
		e("> > > id={0} email={1}".format(id, email))
		camera = Camera.objects.filter(claim_token=id).first()
		if camera is None:
			# TODO: redirect to error message page after few attempts because of eventual consistency
			# raise Exception("CAMERA DELETED")
			return HttpResponseRedirect(reverse('users:expired'))
		user = get_user_model().objects.filter(email=email).first()
		if user is not None:
			return HttpResponseRedirect('user:expired')

		# return self.ok()
		## now we create user account

		auth = get_user_model()()
		auth.email = email
		auth.password = request.POST.get('password_1')
		auth.challange = generate_challange()
		auth.session_key = generate_session_key()
		auth.session_created = datetime.now()
		auth.save()
		# now login new user
		## now we claim its device
		response = self._claim_device(auth, id, email, camera)
		response.set_cookie('session_key', auth.session_key)

		return response


# this is not API call so response shall not be JSON but error or success page redirection
# class ClaimDevice(self, id, owner, **kwargs):
class ClaimDeviceView(ClaimDeviceMixin, ChkincamResponseMixin, View):
	http_method_names = ['get']

	def get(self, request, *args, **kwargs):
		id = base64.urlsafe_b64decode(str(request.GET.get('id')))
		email = str(base64.urlsafe_b64decode(str(request.GET.get('owner')))).lower()
		try:
			camera = Camera.objects.get(claim_token=id)
		except Camera.DoesNotExist:
			# TODO: redirect to error message page after few attempts because of eventual consistency
			# raise Exception("CAMERA DELETED")
			e("CAMERA NOT FOUND claim token={0}".format(id))
			return HttpResponseRedirect(reverse('users:expired'))

		try:
			user = get_user_model().objects.get(email=email)
		except get_user_model().DoesNotExist:
			redirect = HttpResponseRedirect(reverse('users:choose_pass'))
			redirect.set_cookie('id', id)
			redirect.set_cookie('owner', email)
			return redirect

		self._claim_device(user, id, email, camera)

		i("redirecting request to : login page")
		redirect = HttpResponseRedirect("http://chkincam.com")
		return redirect


# TODO: rename push_video into segment_upload_request
# class SegmentUploadRequest(self,**kwargs):  #, utc_start, duration, chkin_version, video_width, video_height, title, description, video_name, thumbnail_width, thumbnail_height, **kwargs):
class SegmentUploadRequest(CameraAuthView):  #, utc_start, duration, chkin_version, video_width, video_height, title, description, video_name, thumbnail_width, thumbnail_height, **kwargs):
	model = Camera
	http_method_names = ['get', 'post']

	##def get(self, request, *args, **kwargs):

	def dispatch(self, request, *args, **kwargs):
		super(SegmentUploadRequest, self).dispatch(request, *args, **kwargs)

		if self.auth and self.auth.owner:
			if not self.auth.owner.subscription_end_time:
				return self.error("NOT SUBSCRIBED")
			is_subscribed = self.auth.owner.subscription_end_time >= datetime.utcnow().replace(tzinfo = pytz.utc)

			if is_subscribed:
				server_address = "http://{host}:{port}".format(
						host=settings.SERVER_HOSTNAME,
						port=settings.SERVER_PORT
				)
				return self.serve_result(
					{  # video_upload_url=blobstore.create_upload_url('/push_video_blobed?id={id}'.format(id=id)),
					   # image_upload_url=blobstore.create_upload_url('/push_image_blobed?id={id}'.format(id=id)),
					   "upload_url": (server_address + reverse('camera:push_data')),
					   # "image_upload_url" : blobstore.create_upload_url('/push_image_blobed'),
					})
			else:
				return self.error("NOT SUBSCRIBED")
		else:
			return ("IDENTITY ERROR")


class OwnSegments(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(OwnSegments, self).dispatch(request, *args, **kwargs)
		camera = self.auth
		return self.serve_result(
			{"segments": camera.segments(request.REQUEST.get('from_time'), request.REQUEST.get('to_time'))})


class CameraSegments(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(CameraSegments, self).dispatch(request, *args, **kwargs)
		"Returns segments of specified camera"
		cam_id = int(key_from_jsonparam(request.REQUEST.get('id')))
		camera = Camera.objects.get(pk=cam_id)
		if camera is None:
			return self.error("BROKEN LINK")
		if not isinstance(camera, Camera):
			return self.error("NOT APPLICABLE")
		# check if this is our camera
		if camera.owner != self.auth:
			return self.error("BROKEN LINK")
		segments = camera.segments(request.REQUEST.get('from_time'), request.REQUEST.get('to_time'))
		return self.serve_result({
			"segments": segments
		})


class UserCameras(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(UserCameras, self).dispatch(request, *args, **kwargs)
		user = self.auth
		if not isinstance(user, User):
			return self.error("NOT APPLICABLE")
		list_of_cameras = []
		cameras = Camera.objects.filter(owner=user)
		for camera in cameras:
			state = camera.params_to_dict()
			## check if camera is online
			if settings.HARTBEAT_PARAMETER_NAME in state:
				hartbeat_time = float(state[settings.HARTBEAT_PARAMETER_NAME])
				if ( (time.time() - hartbeat_time) > settings.HARTBEAT_TIME_INTERVAL ):
					state[settings.ONLINE_PARAMETER_NAME] = 'false'

			list_of_cameras.append({
				"state": state,
				# "id": key_to_jsonparam(camera.key),
				"id": key_to_jsonparam(camera.pk),
				"mac_address": camera.name,  #"segments" : self._all_segments_(camera.key)
			})
		return self.serve_result({
			"cameras": list_of_cameras,
			"subscription_start_time": date_to_timestamp(user.subscription_start_time),
			"subscription_end_time": date_to_timestamp(user.subscription_end_time),
			"your_ip_address": self.request.session.ip,
			"user_email": user.email,
			"phone_number": user.phone_number,
			"telephone_co": user.telephone_co
		})


class SetAccountInfos(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SetAccountInfos, self).dispatch(request, *args, **kwargs)
		user = self.auth
		json_state = request.REQUEST.get('json_state')
		if json_state:
			kwargs = loads(json_state)
			e("account infos using json: {0}".format(repr(kwargs)))
			if not isinstance(kwargs, dict):
				return self.error("MALFORMED_JSON")
		else:
			e("account infos using kwargs: {0}".format(repr(kwargs)))

		for name, value in kwargs.items():
			e("_set_account_infos(camera, {0}, {1})".format(repr(name), repr(value)))
			if name == "phone_number":
				user.phone_number = value
			if name == "telephone_co":
				user.telephone_co = value

		user.save()
		return self.ok()


class SetCameraState(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SetCameraState, self).dispatch(request, *args, **kwargs)
		json_state = request.REQUEST.get('json_state')
		camera_id = request.REQUEST.get('camera_id')
		e("Setting camera state for camera with id : {0}".format(repr(camera_id)))
		send_gcm = request.REQUEST.get('send_gcm') == "true"
		# auth = self._authorize()
		if camera_id is not None:
			camera_id =  int(key_from_jsonparam(request.REQUEST.get('camera_id')))
			# camkey = key_from_jsonparam(camera_id)
			camera = Camera.objects.get(pk=camera_id)
		else:
			camera = self.auth
			if not isinstance(camera, Camera):
				return self.error("NOT APPLICABLE")
		if json_state:
			kwargs = loads(json_state)
			e("setting state using json: {0}".format(repr(kwargs)))
			if not isinstance(kwargs, dict):
				return self.error("MALFORMED_JSON")
			request_dict = dict(kwargs)
		else:
			e("setting state using kwargs: {0}".format(repr(kwargs)))
			request_dict = dict(request.REQUEST)

		gcm_payload, gcm_collapsekey = None, None
		sch_gcm_payload, sch_gcm_collapsekey = None, None

		# do not update from, to values if schedule is inactive   request.REQUEST
		
		if request.REQUEST.get('schedule_active') == "false":
			if request_dict['schedule_from']:
				request_dict.pop('schedule_from')
			if request_dict['schedule_to']:
				request_dict.pop('schedule_to')
			# kwargs.pop('schedule_from', None)
			# kwargs.pop('schedule_to', None)

		i("Set a timezon for camera by ip !!!")
		timezone = None
		try:
			timezone = gic.time_zone_by_addr(request.META.get('REMOTE_ADDR'))
			i("Time zone for camera : " + camera.name + " is : " + timezone + " based on ip : " + request.META.get('REMOTE_ADDR'))
		except Exception, ex:
			e("Error determening timezone for ip : " + request.META.get('REMOTE_ADDR'))
		if not (timezone is None):
			i("Camera new timezone is : " + timezone)
			request_dict["camera_timezone"] = timezone

		for name, value in request_dict.items():
			e("_set_camera_state(camera, {0}, {1})".format(repr(name), repr(value)))
			if name == "recording_mode":
				# detect if change in recording mode happened:
				old = camera.get_camera_state_value("recording_mode")
				if send_gcm and (old is None or old != value):
					# send gc command
					w("[recording_mode changed] old={0} new={1}".format(old, value))
					if value == "true":
						# payload = {"action":"app.chkin.START_RECORDING_MODE"}, collapse_key = start_recording
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.START_RECORDING_MODE"}', 'start_recording'
					else:
						# payload = {"action":"app.chkin.STOP_RECORDING_MODE"}, collapse_key = stop_recording
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.STOP_RECORDING_MODE"}', 'stop_recording'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)
			if name == "microphone":
				if value == "true":
					gcm_payload, gcm_collapsekey = '{"action":"app.chkin.MICROPHONE_ON"}', 'microphone_on'
				else:
					gcm_payload, gcm_collapsekey = '{"action":"app.chkin.MICROPHONE_OFF"}', 'microphone_off'
				camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "email_alert_active":
				old = camera.get_camera_state_value("email_alert_active")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT_ON"}', 'emailalert_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT_OFF"}', 'emailalert_off'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "push_alert_active":
				old = camera.get_camera_state_value("push_alert_active")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT_ON"}', 'pushalert_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT_OFF"}', 'pushalert_off'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "motion_event_alert_email":
				old = camera.get_camera_state_value("motion_event_alert_email")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_email":"true"}', 'motion_event_alert_email_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_email":"false"}', 'motion_event_alert_email_off'
						#TODO send_gcm_message
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "loud_noise_alert_email":
				old = camera.get_camera_state_value("loud_noise_alert_email")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_email":"true"}', 'loud_noise_alert_email_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_email":"false"}', 'loud_noise_alert_email_off'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "motion_event_alert_push":
				old = camera.get_camera_state_value("motion_event_alert_push")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT", "motion_event_alert_push":"true"}', 'motion_event_alert_push_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT", "motion_event_alert_push":"false"}', 'motion_event_alert_push_off'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "loud_noise_alert_push":
				old = camera.get_camera_state_value("loud_noise_alert_push")
				if (old is None or old != value):
					if value == "true":
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT", "loud_noise_alert_push":"true"}', 'loud_noise_alert_push_on'
					else:
						gcm_payload, gcm_collapsekey = '{"action":"app.chkin.PUSHALERT", "loud_noise_alert_push":"false"}', 'loud_noise_alert_push_off'
					camera.send_gcm_message(gcm_payload, gcm_collapsekey)

			if name == "schedule_active":
				if value == "true":
					sch_gcm_collapsekey = 'schedule_on'
					if sch_gcm_payload is None:
						sch_gcm_payload = '"action":"app.chkin.SCHEDULE_ON"'
					else:

						sch_gcm_payload = sch_gcm_payload + ',"action":"app.chkin.SCHEDULE_ON"'

				else:
					sch_gcm_collapsekey = 'schedule_off'
					if sch_gcm_payload is None:
						sch_gcm_payload = '"action":"app.chkin.SCHEDULE_OFF"'
					else:
						sch_gcm_payload = sch_gcm_payload + ',"action":"app.chkin.SCHEDULE_OFF"'

			if name == "schedule_by_days":
				if sch_gcm_payload is None:
					sch_gcm_payload = '"schedule_by_days":"' + value + '"'
				else:
					sch_gcm_payload = sch_gcm_payload + ',"schedule_by_days":"' + value + '"'

			if name == "schedule_from":
				if sch_gcm_payload is None:
					sch_gcm_payload = '"schedule_from":"' + value + '"'
				else:
					sch_gcm_payload = sch_gcm_payload + ',"schedule_from":"' + value + '"'

			if name == "schedule_to":
				if sch_gcm_payload is None:
					sch_gcm_payload = '"schedule_to":"' + value + '"'
				else:
					sch_gcm_payload = sch_gcm_payload + ',"schedule_to":"' + value + '"'

			# if name == "wakeup_auto_monitor":
			# 	if value == "true":
			# 		if not self.is_subscribed(camera.owner):(camera.owner.is_subscribed)
			# 			value = "false"
			# 			i("{0} doesn't have any subscription, Auto monitor is turned OFF".format(camera.user))

			# 	TODO: get android API commands
			# 	if sch_gcm_payload is None:
			# 		sch_gcm_payload = '"wakeup_auto_monitor":"' + value + '"'
			# 	else:
			# 		sch_gcm_payload = sch_gcm_payload + ',"wakeup_auto_monitor":"' + value + '"'
			camera.set_camera_state(name, value)
		e("state after set: {0}".format(repr(camera.get_camera_state())))

		"""
		## Not sure what to do here
		if request.REQUEST.get('schedule_active') == "false":
			# reset the database values id `schedule_active` is inactive
			camera.state = [item for item in camera.state if item.name not in ['schedule_to', 'schedule_from']]
			camera.save()
		"""


		e("sch_gcm_payload: {0}".format(repr(sch_gcm_payload)))
		if sch_gcm_payload is not None:
			sch_gcm_payload = "{" + sch_gcm_payload + "}"
			camera.send_gcm_message(sch_gcm_payload, sch_gcm_collapsekey)

		camera.save()
		return self.ok()


class CheckCameraOffline(CameraAuthView, EmailSmsViewMixin):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(CheckCameraOffline, self).dispatch(request, *args, **kwargs)
		cameras = Camera.objects.all()
		for camera in cameras:
			state = camera.params_to_dict()
			## check if camera is online
			if state[settings.ONLINE_PARAMETER_NAME] == 'true':
				if state.has_key(settings.HARTBEAT_PARAMETER_NAME):
					hartbeat_time = float(state[settings.HARTBEAT_PARAMETER_NAME])
					if ( (time.time() - hartbeat_time) > settings.HARTBEAT_TIME_INTERVAL ):
						camera.set_camera_state(settings.ONLINE_PARAMETER_NAME, "false")
						camera.save()
						if state.has_key("email_alert_active") and state["email_alert_active"] == "true":
							camera_name = ""
							if state.has_key("device_title") and state["device_title"]:
								camera_name = state["device_title"]
							camera = self.auth
							if camera is None:
								continue
							mac_address = camera.name

							user = camera.owner
							receiver = user.name
							phone_number = user.phone_number
							telephone_co = user.telephone_co
							timezone_str = ""
							if state.has_key("camera_timezone_local_area") and state["camera_timezone_local_area"]:
								timezone_str = state["camera_timezone_local_area"]
							if state.has_key("camera_offline_alert_email") and state[
								"camera_offline_alert_email"] == "true":
								self._send_email_sms_alert("offline_alert_email", receiver, phone_number, telephone_co,
														   camera_name, mac_address, camera.key, timezone_str)
							e("camera state: {0}".format(repr(state)))
							if state.has_key("camera_offline_alert_sms") and state[
								"camera_offline_alert_sms"] == "true":
								e("camera_offline_alert_sms")
								self._send_email_sms_alert("offline_alert_sms", receiver, phone_number, telephone_co,
														   camera_name, mac_address, camera.key, timezone_str)

		return self.ok()


class GetCameraState(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		i("Get camera state called !!!");
		super(GetCameraState, self).dispatch(request, *args, **kwargs)

		camera_id = request.REQUEST.get('camera_id')
		if camera_id is not None:
			camera_id =  int(key_from_jsonparam(request.REQUEST.get('camera_id')))
			camera = Camera.objects.get(pk=camera_id)
			# camkey = key_from_jsonparam(camera_id)
			# camera = camkey.get()
		else:
			camera = self.auth
			if not isinstance(camera, Camera):
				return self.error("NOT APPLICABLE")

		ret = camera.params_to_dict()
		## check if camera reported to server in given timeframe
		if ret.has_key(settings.HARTBEAT_PARAMETER_NAME):
			last_hartbeat = float(ret[settings.HARTBEAT_PARAMETER_NAME])
			if ((time.time() - last_hartbeat) > settings.HARTBEAT_TIME_INTERVAL):
				## camera did not reporrt it self on time, declaring it offline
				ret[settings.ONLINE_PARAMETER_NAME] = "false"

		## we do not want to set external ip of the client to the camera state,
		## so we are counting that when the key is sent it is a camera requesting the state,
		## if the id is sent then it is the client.
		if (camera_id == None):
			## compare camera current external ip with actual external ip
			if (ret["external_ip"] != request.META.get('REMOTE_ADDR')):
				camera.set_camera_state("external_ip", request.META.get('REMOTE_ADDR'))
				ret["external_ip"] = request.META.get('REMOTE_ADDR')
		return self.serve_result({"state": ret})


class ResetCameraState(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(ResetCameraState, self).dispatch(request, *args, **kwargs)

		camera_id = request.REQUEST.get('camera_id')
		if camera_id is not None:
			camera = Camera.objects.get(pk=camera_id)
		else:
			camera = self.auth
			if not isinstance(camera, Camera):
				return self.error("NOT APPLICABLE")
		camera.default_camera_state()
		camera.save()
		return self.ok()


# def image(self, id, **kwargs):
class Image(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(Image, self).dispatch(request, *args, **kwargs)
		id = request.REQUEST.get('id')
		try:
			id = base64.urlsafe_b64decode(str(id))
		except:
			return self.error('BROKEN LINK')
		thumbnail_blobinfo = blobstore.BlobInfo.get(id)
		if _authorized_for_asset(thumbnail_blobinfo, self.auth, VideoSegment.thumbnail_url):
			_send_blob(thumbnail_blobinfo, "image/jpeg")
		return ""


		# def video(self, id, **kwargs):


class Video(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(Video, self).dispatch(request, *args, **kwargs)
		id = request.REQUEST.get('id')

		try:
			id = base64.urlsafe_b64decode(str(id))
		except:
			return self.error('BROKEN LINK')
		video_blobinfo = blobstore.BlobInfo.get(id)
		import info

		info.ViewLog.log(video_blobinfo)
		user = self.auth
		# auth = self._authorize()
		if _authorized_for_asset(video_blobinfo, self.auth, VideoSegment.video_url):
			# self._send_blob(video_blobinfo, "video/mp2t")
			_send_blob(video_blobinfo, "video/x-flv")
		return ""


# class SendCommand(self, camera_id, payload, collapse_key=None, expiry_in_seconds=300.0, **kwargs):
class SendCommand(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SendCommand, self).dispatch(request, *args, **kwargs)

		expiry_in_seconds = 300
		if request.REQUEST.has_key('expiry_in_seconds'):
			expiry_in_seconds = float(request.REQUEST.get('expiry_in_seconds'))

		collapse_key = None
		
		if not request.REQUEST.has_key('payload'):
			return self.error("NOT APPLICABLE")

		user = self.auth
		camera_id = request.REQUEST.get('camera_id')
		if camera_id is not None:
			camera_id =  int(key_from_jsonparam(request.REQUEST.get('camera_id')))
			camera = Camera.objects.get(pk=camera_id)
		else:
			return self.error("NOT APPLICABLE")

		if camera.owner.id != user.id:
			# you must own that camera
			return self.error("IDENTITY ERROR")

		e("send_command: {0}".format(repr(request.REQUEST.get('payload'))))
		camera.send_gcm_message(request.REQUEST.get('payload'), collapse_key, expiry_in_seconds)
		return self.ok()


class RemoveCameraFromUser(UserAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(RemoveCameraFromUser, self).dispatch(request, *args, **kwargs)
		camkey = request.REQUEST.get('camera_id')
		camera = Camera.objects.get(pk=camkey)
		if not request.REQUEST.get('admin_mode'):
			user = self.auth
			if camera.owner != user:
				# you must own that camera
				return self.error("IDENTITY ERROR")
		else:
			guser = request.user
			if not guser or not guser.is_superuser:
				# you must be an admin
				return self.error("IDENTITY ERROR")
		if camera:
			camera.deleted = False
			camera.save()

			# taskqueue.add(url='/dashboard/users/camera_increment?key=%s&rev=true' % (camera.owner.urlsafe()),
			#			   method='GET')
			# cam_auth.key.delete()
			# task_result = URL('http://{domain}/dashboard/users/camera_increment'.format(
			#	 domain=Site.objects.get_current().domain)
			# ).get_async(key=base64.urlsafe_b64encode(str(camera.owner.pk)), rev='true')

			return self.ok()
		else:
			return self.error("MISSING LINK")

			# return _remove_camera_from_user(camera)


class RegisterDevice(ChkincamResponseMixin, View):
	http_method_names = ['get', 'post']
	model = Camera

	@transaction.atomic
	def dispatch(self, request, *args, **kwargs):
		"""
			IDH - independant design house
			so we need to map given range of SSID & MAC to allowed devices

			SSID & MAC are aquired from IDH
		"""
		password = request.REQUEST.get('password')
		name = request.REQUEST.get('name')

		if not request.REQUEST.get('push_token'):
			push_token = request.REQUEST.get('password')
		query = Camera.objects.filter(name=name)

		if query.exists():
			e("DUPLICATE device registration name={n} pass={p}".format(n=name, p=password))
			# return self.error('DUPLICATE')
			return self.error('DUPLICATE')

		try:
			camera = Camera()
			camera.build_id = request.REQUEST.get('build_id', 'USA')
			camera.name = name
			camera.save()

			camera.default_camera_state()
			camera.password = password
			camera.challenge = generate_challange()
			camera.set_camera_state("push_token", request.REQUEST.get('push_token'))
			camera.save()
		except Exception, ex:
			return self.error(ex.message)

		return self.ok()


class CameraHeartbeat(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(CameraHeartbeat, self).dispatch(request, *args, **kwargs)
		cam = self.auth
		cam.set_camera_state(settings.ONLINE_PARAMETER_NAME, "true")
		current_time = str(time.time())
		cam.set_camera_state(settings.HARTBEAT_PARAMETER_NAME, current_time)
		cam.save()
		return self.ok()


class IsCameraClaimed(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(IsCameraClaimed, self).dispatch(request, *args, **kwargs)
		cam = self.auth
		user = get_object_or_None(get_user_model(), email=request.REQUEST.get('name'))
		if cam.owner:
			if cam.owner == user:
				return self.ok()
		return self.error("NOT APPLICABLE")


class GetUsernameForClaimedCamera(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(GetUsernameForClaimedCamera, self).dispatch(request, *args, **kwargs)
		camera = self.auth
		if not camera.owner:
			return self.error("NOT APPLICABLE")
		else:
			return self.serve_result({
				"camera_owner": camera.owner.email,
			})


# def remove_video_from_user(self, video_id, admin_mode=False, **kwargs):
class RemoveVideoFromUser(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(RemoveVideoFromUser, self).dispatch(request, *args, **kwargs)
		# video_key = key_from_jsonparam(video_id)
		video = VideoSegment.objects.get(pk=request.REQUEST.get('video_id'))

		if not request.REQUEST.get('admin_mode'):
			super(RemoveVideoFromUser, self).dispatch(request, *args, **kwargs)
			if video.user != self.auth.owner:
				# you must own that camera
				return self.error("IDENTITY ERROR")
		else:
			guser = request.user
			if not guser or not guser.is_staff:
				# you must be an admin
				return self.error("IDENTITY ERROR")

		if video.video_url != None:
			# http://celery.readthedocs.org/en/latest/userguide/remote-tasks.html#enabling-the-http-task
			task_result = URL('http://{domain}/dashboard/users/media_increment'.format(
				domain=Site.objects.get_current().domain)
			).get_async(key=base64.urlsafe_b64encode(str(video.user.pk)), size=video.size, rev='true')

			# taskqueue.add(
			#	 url='/dashboard/users/media_increment?key=%s&size=%s&rev=true' % (video.user.urlsafe(), info.size),
			#	 method='GET')

		if video.thumbnail_url is not None:
			video.thumbnail_url.delete()
		video.deleted = True
		video.save()

		return self.ok()


class PushData(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(PushData, self).dispatch(request, *args, **kwargs)
		camera_id = request.REQUEST.get("camera_id")
		camera_id =  int(key_from_jsonparam(request.REQUEST.get('camera_id')))
		camera = Camera.objects.get(pk=camera_id)
		video_data = None
		if request.FILES.has_key("video_data"):
			video_data = request.FILES["video_data"]

		thumbnail_data = None
		if request.FILES.has_key("thumbnail_data"):
			thumbnail_data = request.FILES["thumbnail_data"]

		vs = VideoSegment()
		vs.owner = camera
		vs.utc_start =  datetime.fromtimestamp(float(request.REQUEST.get("utc_start")))
		vs.chkin_version = request.REQUEST.get("utc_start")
		vs.user = camera.owner
		vs.size = readProperty(request.REQUEST, "size", "0")
		vs.description = readProperty(request.REQUEST, "description", "")
		vs.duration = readProperty(request.REQUEST, "video_duration", "0")

		if video_data != None:
			i("Uploading video ...")
			video_url = storageManager.generate_file_location(camera.name)
			vs.video_width = readProperty(request.REQUEST, "video_width", "")
			vs.video_height = readProperty(request.REQUEST, "video_height", "")
			vs.video_title = readProperty(request.REQUEST, "title", "")
			vs.video_name = readProperty(request.REQUEST, "video_name", "")
			vs.video_url = video_url
			video_file = open(os.path.join(settings.STORAGE_LOCATION, video_url), "w")
			for chunk in video_data.chunks():
				video_file.write(chunk)

		if thumbnail_data != None: 
			i("Uploading thumbnail ...")
			thumbnail_url = storageManager.generate_file_location(camera.name)
			vs.thumbnail_width = readProperty(request.REQUEST, "thumbnail_width", "-1")
			vs.thumbnail_height = readProperty(request.REQUEST, "thumbnail_height", "-1")
			vs.thumbnail_url = thumbnail_url
			thumbnail_file = open(os.path.join(settings.STORAGE_LOCATION, thumbnail_url) , "w")
			for chunk in thumbnail_data.chunks():
				thumbnail_file.write(chunk)

		vs.save()
		return self.ok();

from wsgiref.util import FileWrapper
class PullData(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(PullData, self).dispatch(request, *args, **kwargs)
		
		relative_path = request.path.replace("/pull_data/", "")

		if isinstance(self.auth, User):
			## todo check if video belong to camera owned by this user
			camera_name = relative_path.split("/")[0]
			video_name = request.REQUEST.get("video_name")
			camera_name = key_from_jsonparam(camera_name)
			camera = Camera.objects.get(name=camera_name)
			if camera.owner != self.auth:
				return self.error("NOT APPLICABLE")

			rfile_path = "{record_dir}/{relative_path}".format(
				record_dir=settings.STORAGE_LOCATION,
				relative_path=relative_path
			)

			file=FileWrapper(open(rfile_path, 'rb'))
			response = HttpResponse(file, content_type='video/x-flv')
			response['Content-Disposition'] = "attachment; filename={video_name}".format(
				video_name=video_name
			)
			return response
		else:
			return self.error("IDENTITY ERROR")
		
		
def readProperty(request, key, default):
	if request.has_key(key):
		return request.get(key)
	return default
