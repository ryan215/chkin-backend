from django.conf.urls import url
from apps.camera.views import ClaimDeviceRequest, EnterCredentials, ClaimDeviceView, SegmentUploadRequest, OwnSegments, \
	CameraSegments, UserCameras, SetAccountInfos, SetCameraState, CheckCameraOffline, GetCameraState, \
	ResetCameraState, Image, Video, SendCommand, RemoveCameraFromUser, RegisterDevice, IsCameraClaimed, \
	GetUsernameForClaimedCamera, RemoveVideoFromUser, PullData, PushData, CameraHeartbeat

urlpatterns = [
	url(r'^claim_device_request/$', ClaimDeviceRequest.as_view(), name='user_view'),
	url(r'^enter_credentials/$', EnterCredentials.as_view(), name='enter_credentials'),
	url(r'^claim_device/$', ClaimDeviceView.as_view(), name='claim_device'),
	url(r'^segment_upload_request/$', SegmentUploadRequest.as_view(), name='segment_upload_request'),
	url(r'^own_segments/$', OwnSegments.as_view(), name='own_segments'),
	url(r'^camera_segments/$', CameraSegments.as_view(), name='camera_segments'),
	url(r'^user_cameras/$', UserCameras.as_view(), name='user_cameras'),
	url(r'^set_account_infos/$', SetAccountInfos.as_view(), name='set_account_infos'),
	url(r'^set_camera_state/$', SetCameraState.as_view(), name='set_camera_state'),
	url(r'^check_camera_offline/$', CheckCameraOffline.as_view(), name='check_camera_offline'),
	url(r'^get_camera_state/$', GetCameraState.as_view(), name='get_camera_state'),
	url(r'^reset_camera_state/$', ResetCameraState.as_view(), name='get_camera_state'),
	url(r'^image/$', Image.as_view(), name='image'),
	url(r'^video/$', Video.as_view(), name='video'),
	url(r'^send_command/$', SendCommand.as_view(), name='send_command'),
	url(r'^remove_camera_from_user/$', RemoveCameraFromUser.as_view(), name='remove_camera_from_user'),
	url(r'^register_device/$', RegisterDevice.as_view(), name='register_device'),
	url(r'^is_camera_claimed/$', IsCameraClaimed.as_view(), name='is_camera_claimed'),
	url(r'^get_username_for_claimed_camera/$', GetUsernameForClaimedCamera.as_view(), name='get_username_for_claimed_camera'),
	url(r'^remove_video_from_user$', RemoveVideoFromUser.as_view(), name='remove_video_from_user'),
	url(r'^push_data/$', PushData.as_view(), name='push_data'),
	url(r'^camera_heartbeat/$', CameraHeartbeat.as_view(), name='camera_heartbeat'),
	url(r'^pull_data/.*$', PullData.as_view(), name='pull_data'),
]
