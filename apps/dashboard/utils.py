from datetime import datetime
from datetime import timedelta
from apps.users.models import User

storage_cost = 0.026
downloaded_cost = 0.12
day = timedelta(1)
month = timedelta(30)
default_cost = {'daily': 0.0, 'weekly': 0.0, 'monthly': 0.0}

default_usage = {
'storage': {'daily': 0, 'weekly': 0, 'monthly': 0},
'downloaded': {'daily': 0, 'weekly': 0, 'monthly': 0},
}

def calculate_cost(storage, downloaded):
    return storage * storage_cost / 1073741824.0 + downloaded * downloaded_cost / 1073741824.0

class BaseInfo(object):
    pass

class UserInfo(BaseInfo):
    @classmethod
    def summary(cls):
        resp = []
        for ent in User.objects.filter(deleted = False):
            if ent.usage != None:
                cost = {
                'daily': calculate_cost(ent.usage.storage.daily, ent.usage.downloaded.daily),
                'weekly': calculate_cost(ent.usage.storage.weekly, ent.usage.downloaded.weekly),
                'monthly': calculate_cost(ent.usage.storage.monthly, ent.usage.downloaded.monthly),
                }
                usage = ent.usage.json()
            else:
                cost = default_cost
                usage = default_usage

            resp.append({'key': ent.session_key, 'id': ent.name, 'camera_count': ent.camera_count,
                         'media_count': ent.media_count, 'storage_used': ent.storage_used,
                         'deleted': ent.deleted == True, 'usage': usage, 'cost': cost})
        return resp