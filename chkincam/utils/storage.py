import os
import uuid

from django.conf import settings
from chkincam.utils import key_to_jsonparam, key_from_jsonparam




class StorageManager():
	
	def generate_file_location(self, client_name):
		"""
			All files uploaded by same entetty will be stored under
			same directory splitted in to several subdirectories.
			make sure that process have all permission needed to
			read and write to storage directory.
		"""
		client_dir_name = key_to_jsonparam(client_name) 
		""" 
			Generate the relative file path by creating UUID, first 6 characters will be splites
			in 3 subdirectories to make sure that we do not overcrowd the file system with too many
			files in one place. 
		"""
		file_uuid = str(uuid.uuid4())
		relative_path = os.path.join(client_dir_name, file_uuid[0:2], file_uuid[2:4], file_uuid[4:6])
		file_name = file_uuid[6:]
		full_file_path = os.path.join(settings.STORAGE_LOCATION,  relative_path)
		os.makedirs(full_file_path, 0755);
		return os.path.join(relative_path, file_name)
			

storageManager = StorageManager();
