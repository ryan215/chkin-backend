interface VisStatic{
	Timeline(container: any, items: any, options: any): void;
	DataSet(data: any): void;
}

declare var vis: VisStatic;