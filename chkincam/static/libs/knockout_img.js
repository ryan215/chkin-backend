ko.bindingHandlers.img = {
	update: function (element, valueAccessor) {
		//grab the value of the parameters, making sure to unwrap anything that could be observable
		var value    = ko.utils.unwrapObservable(valueAccessor()),
			src      = ko.utils.unwrapObservable(value.src),
			fallback = ko.utils.unwrapObservable(value.fallback),
			$element = $(element);

		//now set the src attribute to either the bound or the fallback value
		if (src) {
			$element.attr("src", src);
		} else {
			$element.attr("src", fallback);
		}
	},
	init: function (element, valueAccessor, allBindingsAccessor, data, context) {
		var $element = $(element);
		var c = 0;
		//hook up error handling that will unwrap and set the fallback value
		$element.error(function () {
			var value = ko.utils.unwrapObservable(valueAccessor()),
				fallback = ko.utils.unwrapObservable(value.fallback),
				count = data.thumb_fallback_count(),
				finalFallback = ko.utils.unwrapObservable(value.finalFallback);
			if(count > 0){
				data.thumb_fallback_count(count - 1);
				$element.attr("src", fallback);
				console.log("-------------------fallback", count - 1, data.mac_address());
			}else{
				console.log("finalFallback", data.mac_address());
				$element.attr("src", finalFallback);
			}
		});
	}
};