window.ko.bindingHandlers.timepicker = {
	init: function(element, valueAccessor, allBindingsAccessor) {
		$(element).parent().datetimepicker({pickDate: false});

		window.ko.utils.registerEventHandler($(element).parent(), "change.dp", function (event) {
			var value = valueAccessor();
			if (window.ko.isObservable(value)) {
				var date = $(element).parent().data("DateTimePicker").getDate();
				date = moment(date).toDate()
				var hours = date.getHours();
				var minutes = date.getMinutes();
				var ampm = hours >= 12 ? 'PM' : 'AM';
				hours = hours % 12;
				hours = hours ? hours : 12; // the hour '0' should be '12'
				minutes = minutes < 10 ? '0'+minutes : minutes;
				var strTime = hours + ':' + minutes + ' ' + ampm;

				console.log("new data time: ", date);
//				value(moment(thedate).toDate());
				value(strTime);
			}
		});
	},
	update: function(element, valueAccessor) {
		var widget = $(element).parent().data("DateTimePicker");
		//when the view model is updated, update the widget
		//var thedate = new Date(parseInt(window.ko.utils.unwrapObservable(valueAccessor()).toString().substr(6)));
		var val = window.ko.utils.unwrapObservable(valueAccessor());
		if(val !== undefined){
			widget.setDate(val);
		}
	}
};
