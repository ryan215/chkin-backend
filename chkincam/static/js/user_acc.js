CookieUtils/**
 * user_acc page logic
 */

var panel;
var camerasView;

//var test_key = "ce8b8989-4ea1-4863-a3ff-f7d94bf19657";
var test_key = "07b3989f-da74-404c-96d5-5a4cb51db0df";
var IP = "";
var schedule_panel_available = false;

//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON NETWORK INFO PANEL
var local_ip = "";
var local_port = "";
var external_ip = "";
var external_port = "";
//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON CAMERA TIMER PANEL
var from_time = "";
var to_time = "";
var days = [];
//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON ALERT PANEL
var motion_event_alert_email = false;
var loud_noise_alert_email = false;
var camera_offline_alert_email = false;

var motion_event_alert_sms = false;
var loud_noise_alert_sms = false;
var camera_offline_alert_sms = false;

//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON SETTINGS PANEL
var deviceTitle = "";
var microphone = false;
var wakeup_auto_monitor = false;
//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON ACCOUNT PANEL
var phone_number = "";
var telephone_co = "";

//BACKUP VALUES IN CASE THAT USER CLICK ON CANCEL ON ACCOUNT PANEL
var rec_mod = false;
var url_live = "http://{{IP}}:{{PORT}}/live/{{KEY}}";

var url_current_download = "";
var url_download_filename = "";

var current_cam_json_state = false;

$(document).ready(function () {
	//Load Camera Tabs
	camera_tabs();

	// INIT ON/OFF BUTTONS ON GENERAL CAMERA SETTING
	$.fn.bootstrapSwitch.defaults.size = 'mini';
	$("[name='rec_cam_on_of']").bootstrapSwitch("size", "mini");
	$("[name='settings_cam_on_of']").bootstrapSwitch("size", "mini");

	// DISABLE DROP DOWN CLOSING
	$("#settings_drop_panel").mouseenter(function() {
		$("#settings_drop").attr("data-toggle","");
	}).mouseleave(function() {
		$("#settings_drop").attr("data-toggle","dropdown");
	});

	$("#account_drop_panel").mouseenter(function() {
		$("#account_drop").attr("data-toggle","");
	}).mouseleave(function() {
		$("#account_drop").attr("data-toggle","dropdown");
	});

	//Download Selected Recording
	/*$("#camera-download-button").click(function() {
		alert("Downloading..."+ url_current_download);
		if (url_current_download.length > 0) {
			downloadURI(url_current_download, "helloWorld.flv");
		}
	});*/

	var toggle_rec = 0;

	$("#on-off-rec-cam").click(function() {
		if (toggle_rec == 0) {
			$(this).css({"background-color": "red", "color": "#fff"});
			toggle_rec = 1;
		} else {
			$(this).css({"background-color": "#fff", "color": "red"});
			toggle_rec = 0;
		}
		on_off_rec();
	});

	//Change Pass Button
	$("#user-change-pass").click(function() {
		$("#user-password-cont").slideToggle();
	});

	//Subscription
	$("#change-user-plan-btn").click(function() {
		$("#change-user-plan-cont").slideToggle();
	});

	// SETTINGS PANEL LOGIC
	$("#settings_holder").on('hide.bs.dropdown', function(e){
		$(".settings_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;
		//save_general();
	}).on('show.bs.dropdown', function(){
		deviceTitle = camerasView.selected().state.device_title();
		microphone = camerasView.selected().state.microphone();
		wakeup_auto_monitor = camerasView.selected().state.wakeup_auto_monitor();

		$("#settings_panel").removeClass('inactive_panel').addClass('active_panel');
		schedule_panel_available = true;
	});

	// ACCOUNT PANEL
	$("#account_holder").on('hide.bs.dropdown', function(e){
		$(".account_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;
	}).on('show.bs.dropdown', function(){
		phone_number = camerasView.phone_number();
		telephone_co = camerasView.telephone_co();

		$("#account_panel").removeClass('inactive_panel').addClass('active_panel');
		schedule_panel_available = true;
	});
	
	
	// SAVE/CLOSE SETTING LOGIC
	$("#cancel_settings_info").on("click", function(e){
		// RETURN OLD VALUES
		
		camerasView.selected().state.device_title(deviceTitle);
		camerasView.selected().state.microphone(microphone);
		camerasView.selected().state.wakeup_auto_monitor(wakeup_auto_monitor);

		console.log("settings cancel click");

		$("#settings_holder").removeClass("open");
		$(".settings_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;
	});

	$(".save_settings_info").on("click", function(e){
		console.log("settings save click");

		$("#settings_holder").removeClass("open");
		$(".settings_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;

		save_general();
		$(".camera-tabs li.active a").html(camerasView.selected().state.device_title());
		$("#camera-name").html(camerasView.selected().state.device_title());
		$(".modal").modal("hide");
	});

	// SAVE/CLOSE ACCOUNT
	$("#cancel_account").on("click", function(e){
		
		camerasView.phone_number(phone_number);
		camerasView.telephone_co(telephone_co);

		console.log("account cancel click");

		$("#account_holder").removeClass("open");
		$(".account_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;
	});

	$(".save_account_info").on("click", function(e){
		console.log("account save click");

		$("#account_holder").removeClass("open");
		$(".account_panel").addClass('inactive_panel').removeClass('active_panel');
		schedule_panel_available = false;

		save_account();
		$(".camera-tabs li.active a").html(camerasView.selected().state.device_title());
		$("#camera-name").html(camerasView.selected().state.device_title());
		$(".modal").modal("hide");
	});

	// NETWORK INFO LOGIC
	$(".style_network_info>a").on("click", function(){
		local_ip = camerasView.selected().state.internal_ip();
		local_port = camerasView.selected().state.internal_port();
		external_ip = camerasView.selected().state.external_ip();
		external_port = camerasView.selected().state.external_port();
		$("#settings_panel").addClass('inactive_panel').removeClass('active_panel');
		$(".network_info").removeClass('inactive_panel').addClass('active_panel');
	});

	$("#cancel_network_info").on("click", function(e){
		// RETURN OLD VALUES
		camerasView.selected().state.internal_ip(local_ip);
		camerasView.selected().state.internal_port(local_port);
		camerasView.selected().state.external_ip(external_ip);
		camerasView.selected().state.external_port(external_port);
		console.log("cancel click");
		return_to_settings_panel($(".network_info"));
	});

	$("#save_network_info").on("click", function(e){
		console.log("save click");
		return_to_settings_panel($(".network_info"));
	});

	// CAMERA TIMER LOGIC
	$("#save_timer_changes").on("click", function(e){
		console.log("TIMER save click");
		return_to_settings_panel($(".camera_timer"));
		save_general();
	});

	$("#cancel_timer_changes").on("click", function(e){
		console.log("TIMER cancel click");
		// RETURN OLD VALUES
		console.log("old vals: ", from_time, to_time,  days);
		camerasView.selected().state.schedule_from(from_time);
		camerasView.selected().state.schedule_to(to_time);
		camerasView.selected().state.schedule_by_days(days);
		return_to_settings_panel($(".camera_timer"));
	});

	// MAIL ALERT LOGIC
	$("#save_mail_alert_changes").on("click", function(e){
		console.log("MAIL ALERT save click");
		return_to_settings_panel($("#email_alerts"));
	});

	$("#cancel_mail_alert_changes").on("click", function(e){
		console.log("MAIL ALERT cancel click");
		// RETURN OLD VALUES
		camerasView.selected().state.motion_event_alert_email(motion_event_alert_email);
		camerasView.selected().state.loud_noise_alert_email(loud_noise_alert_email);
		camerasView.selected().state.camera_offline_alert_email(camera_offline_alert_email);

		camerasView.selected().state.motion_event_alert_sms(motion_event_alert_sms);
		camerasView.selected().state.loud_noise_alert_sms(loud_noise_alert_sms);
		camerasView.selected().state.camera_offline_alert_sms(camera_offline_alert_sms);

		return_to_settings_panel($("#email_alerts"));
	});

	//DELETE CAMERA LOGIC
	$("#delte_cam_btn").on("click", function(){
		$("#settings_panel").removeClass('active_panel').addClass('inactive_panel');
		$("#delete_camera").removeClass('inactive_panel').addClass('active_panel');
	});

	$("#cancel_delete_camera").on("click", function(e){
		console.log("DELETE CAMERA cancel click");
		// RETURN OLD VALUES
		return_to_settings_panel($("#delete_camera"));
	});

	$("#delete_cam_btn").on("click", function(){
		console.log("delete this cam!!");
		return_to_settings_panel($("#delete_camera"));
		Communication.delete_camera_confirmed();

		//DELETE CAM FORM VIEW MODEL
		camerasView.removeCamera(camerasView.selected().id());

		if(camerasView.cameras().length === 0){
			main.set_camera(new CameraData(empty_data));
		}else{
			main.set_camera(camerasView.cameras()[0]);
		}
	});

	// CHANGE PASSWORD LOGIC
	$("#change_pass").on("click", function(e){
		$("#account_panel").removeClass('active_panel').addClass('inactive_panel');
		$("#passwords_panel").removeClass('inactive_panel').addClass('active_panel');
	});

	$("#change_password").on("click", function(e){
		//TODO: CALL SERVER WITH NEW PASS
		$("#user-password-cont").slideToggle();
		return_to_account_panel();
	});

	$("#cancel_change_password").on("click", function(e){
		return_to_account_panel();
	});

	$("#test_sms").on("click", function(e) {
		console.log("Test SMS click");
		main.test_sms(false);
	});

	// CVR SUBSCRIPTION LOGIC
	$("#close_cvr_subscription").on("click", function(e){
		console.log("CVR subscription close click");
		return_to_account_panel();
	});

	$("#pay_month_plan").on("click", function(e) {
		console.log("CVR subscription month plan click");
		purchase_subscription("month");
	});

	$("#pay_year_plan").on("click", function(e) {
		console.log("CVR subscription year plan click");
		purchase_subscription("year");
	});

	var key = CookieUtils.readCookie("key");
	$.get(DB_SERVER_URL + '/number_of_connected_clients/?key=' + key, function(data){
		var _data = JSON.parse(data);
		$('#no_of_users').text(_data.count);
		// if (_data.sessions.length != 0) {
		// 	$('#last_login_ip').text(_data.sessions[0].ip_address);
		// } else {
		// 	$('#last_login_ip').text("Not available");
		// }
		if (_data.last_session_ip) {
			$('#last_login_ip').text(_data.last_session_ip);
		} else {
			$('#last_login_ip').text("Not available");
		}
	});	

    if ($('.settings_panel').hasClass('inactive_panel')) {
    		if($('.bootstrap-datetimepicker-widget.dropdown-menu').hasClass('picker-open')){
    			$('.bootstrap-datetimepicker-widget.dropdown-menu').css('display', 'none');
    		}
    }

 //    $('#schedule_switch').on('switchChange.bootstrapSwitch', function(event, state) {
	//   console.log(this); // DOM element
	//   console.log(event); // jQuery event
	//   console.log(state); // true | false
	// });
});

var return_to_account_panel = function(){
	camerasView.selected().pass1("");
	camerasView.selected().pass2("");
	$("#passwords_panel").removeClass('active_panel').addClass('inactive_panel');
	$("#cvr_subscription").removeClass('active_panel').addClass('inactive_panel');
	$("#account_panel").removeClass('inactive_panel').addClass('active_panel');
};

var return_to_settings_panel = function(current_panel){
	$("#settings_panel").removeClass('inactive_panel').addClass('active_panel');
	current_panel.removeClass('active_panel').addClass('inactive_panel');
};

var save_scheduler_values = function(){
	days = [];
	var temp = camerasView.selected().state.schedule_by_days()
	$.each(temp, function(index, item){
		days.push(item);
	});
	from_time = camerasView.selected().state.schedule_from();
	to_time = camerasView.selected().state.schedule_to();

	//if(camerasView.selected().state.schedule_active()){
		console.log("saved vals: ", from_time, to_time,  days);
		$("#settings_panel").addClass('inactive_panel').removeClass('active_panel');
		$("#cameraTimer").addClass('active_panel').removeClass('inactive_panel');
	//}
};

var save_mail_alert_values = function(){
	console.log("saved mail_alert_ vals: ");

	motion_event_alert_email = camerasView.selected().state.motion_event_alert_email();
	loud_noise_alert_email = camerasView.selected().state.loud_noise_alert_email();
	camera_offline_alert_email = camerasView.selected().state.camera_offline_alert_email();

	motion_event_alert_sms = camerasView.selected().state.motion_event_alert_sms();
	loud_noise_alert_sms = camerasView.selected().state.loud_noise_alert_sms();
	camera_offline_alert_sms = camerasView.selected().state.camera_offline_alert_sms();

	//if(camerasView.selected().state.email_alert_active()){
		console.log("saved vals: ", from_time, to_time,  days);
		$("#settings_panel").addClass('inactive_panel').removeClass('active_panel');
		$("#email_alerts").addClass('active_panel').removeClass('inactive_panel');
	//}
};

var cvrsubscription_values = function(){
	console.log("cvrsubscription_ vals: ");

	$("#account_panel").addClass('inactive_panel').removeClass('active_panel');
	$("#cvr_subscription").addClass('active_panel').removeClass('inactive_panel');
};

/**
 * Object is just demonstration of timeline usage
 */
var demo;


/**
 * Fake data for knockout.js until real data is not available
 * @type {{selected_camera: {state: {external_ip: string, external_port: string, internal_ip: string, internal_port: string, microphone: string, motion_sensor: string, night_vision: night_vision, recording_quality: string, speaker: string}}}}
 */
var main = {
	selected_camera: {
		state: {
			external_ip: "",
			external_port: "",
			internal_ip: "",
			internal_port: "",
			microphone: "",
			motion_sensor: "",
			night_vision: function () {
				return "";
			},
			recording_quality: "",
			recording_mode: "",
			speaker: "",
			wakeup_auto_monitor: "",
		}
	}
};

function position_to_last_recording(show_notifications) {
	if (__camera_state_fetched[camerasView.selected().id()] == undefined) {
		console.log("position_to_last_recording waiting ...");
		setTimeout(function () {
			position_to_last_recording(show_notifications)
		}, 300);
		// delay this function
		return;
	}

	var camera_segments = camerasView.selected().segments();
	if (camera_segments == undefined || camera_segments.length == 0) {
		if (show_notifications == true) notif({type: "error", msg: "Selected camera has no recordings", position: "center", opacity: 0.8});
		var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000
		console.log(local_timezone_offset_ms, "<===local_timezone_offset_ms");
		var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;
		console.log(camera_timezone_ms, "<===camera_timezone_ms");
		var now_in_camera_timezone = (new Date()).getTime() + local_timezone_offset_ms + camera_timezone_ms;
		console.log(now_in_camera_timezone, "<===now_in_camera_timezone");
		console.log(" ! moveCursor position_to_last_recording no segments", now_in_camera_timezone)
		main.demo.moveCursor(now_in_camera_timezone);
		main.demo.MoveWindowToCursor();
		return;
	}
	if (show_notifications == true) notif({type: "success", msg: "Showing last recording", position: "center", opacity: 0.8});
	var cur = server_time_to_camera_timezone(camera_segments[0].utc_start);
	console.log
	setTimeout(function () {
		console.log(" moveCursor delayed", cur)
		main.demo.moveCursor(cur);
		main.demo.MoveWindowToCursor();
	}, 0);
}

function switch_to_recordings() {
	_stop_player();
	position_to_last_recording(true);
}

/**
 * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
 * by testing for a 'synthetic=true' property on the event object
 * @param {HTMLNode} node The node to fire the event handler on.
 * @param {String} eventName The name of the event without the "on" (e.g., "focus")
 */
function fireEvent(node, eventName) {
	// Make sure we use the ownerDocument from the provided node to avoid cross-window problems
	var doc;
	if (node.ownerDocument) {
		doc = node.ownerDocument;
	} else if (node.nodeType == 9) {
		// the node may be the document itself, nodeType 9 = DOCUMENT_NODE
		doc = node;
	} else {
		throw new Error("Invalid node passed to fireEvent: " + node.id);
	}

	if (node.dispatchEvent) {
		// Gecko-style approach (now the standard) takes more work
		// Gecko-style approach (now the standard) takes more work
		var eventClass = "";

		// Different events have different event classes.
		// If this switch statement can't map an eventName to an eventClass,
		// the event firing is going to fail.
		switch (eventName) {
			case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
			case "mousedown":
			case "mouseup":
				eventClass = "MouseEvents";
				break;

			case "focus":
			case "change":
			case "blur":
			case "select":
				eventClass = "HTMLEvents";
				break;

			default:
				throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
				break;
		}
		var event = doc.createEvent(eventClass);

		var bubbles = eventName == "change" ? false : true;
		event.initEvent(eventName, bubbles, true); // All events created as bubbling and cancelable.

		event.synthetic = true; // allow detection of synthetic events
		// The second parameter says go ahead with the default action
		node.dispatchEvent(event, true);
	} else if (node.fireEvent) {
		// IE-old school style
		var event = doc.createEventObject();
		event.synthetic = true; // allow detection of synthetic events
		node.fireEvent("on" + eventName, event);
	}
};

function save_camera_settings(click_event) {
	click_event.preventDefault && click_event.preventDefault();
	var device_title = document.getElementById("device_title").value;
	var device_adress = document.getElementById("device_adress").value;
	// var device_note = document.getElementById("device_note").value
	var camera_timezone = document.getElementById("camera_timezone").value;
	var key = CookieUtils.readCookie("key");
	$.ajax(DB_SERVER_URL + "/set_camera_state?key=" + key, {type: "POST", dataType: "json",
		data: {
			camera_id: camerasView.selected().id(),
			device_title: device_title,
			// device_adress : device_adress,
			// device_note : device_note,
			camera_timezone: camera_timezone
		},
		success: function (data, textStatus, jqXHR) {
			// display success notif
			if (data.error_code != 0) {
				notif({type: "error", msg: "Error while storing new settings (" + data.error_code + ")", position: "center", opacity: 0.8});
				return;
			}
			notif({type: "success", msg: "New settings stored", position: "center", opacity: 0.8});
			// document.getElementById("camera_set").style.display = 'none';
			fireEvent(document.getElementById("camera_set_close_button"), "click");  // << TODO: johney kako da zatvorim dialog ?
		}
	}).fail(function () {
		notif({type: "error", msg: "Error while storing new settings", position: "center", opacity: 0.8});
	});
}

function purchase_subscription(plan) {	
	//document.location = "/pay?start=" + (__subscription_start_time / 1000.0) + "&end=" + (__subscription_end_time / 1000.0) + "&cams=" + __subscription_camera_count + "&total=" + (__subscription_camera_count * __subscription_price_per_camera)
	var key = CookieUtils.readCookie("key");
	$.get(DB_SERVER_URL + '/session_expired?key=' + key, function(data){
		if (JSON.parse(data)['result']) {
			notif({type: "success", msg: "Redirecting to PayPal site, this may take few minutes", position: "center", opacity: 0.8});
			document.location = DB_SERVER_URL + "/pay?plan=" + plan + "&key=" + key;
		} else {
			setTimeout(function(){
	        	notif({type: "error", msg: "Session has Expired. Please login again", position: "center", opacity: 0.8});
	        }, 300);
			logout();
		}
	});
}

var __is_live_stream = false;
var __current_playitem_start = null;
var __current_playitem_duration = null;
var ___flash_ready = false;
var ___camera_ready = false;
var __my_ip_address = "";
var __subscription_start_time = null
var __subscription_end_time = null
var __subscription_camera_count = null
var __subscription_price_per_camera = 10
var __camera_state_fetched = {}
var ___video_stopped = true
var PLAYER_BUFFER_TIME = 10.0

var __irisplayer_fullscreen = false;

function irisplayer_ready() {
	___flash_ready = true;
	var player = document.getElementById("IrisPlayer");
	console.log("Setting buffer to", PLAYER_BUFFER_TIME);
	player.set_buffer_time(PLAYER_BUFFER_TIME);
	if (___camera_ready == true) {
		// flash is redy later than data
		console.log("start live stream");
		main.go_live();
	}
}

function irisplayer_playback_complete() {
	console.log("playback_complete");
	if (__is_live_stream == true) {
		camerasView.selected().is_live(false);
	}
}

function irisplayer_current_time_changed(time) {
	//console.log("time", time);
	if (___video_stopped == true) return;

	var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
	var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;
	if (__is_live_stream == true) {
		// TODO: convert to camera timezone
		// main.demo.moveCursor(new Date().getTime());
		var now_in_camera_timezone = (new Date()).getTime() + local_timezone_offset_ms + camera_timezone_ms;
		//console.log(" ! moveCursor irisplayer_current_time_changed live", now_in_camera_timezone);
		main.demo.moveCursor(now_in_camera_timezone);
	} else {
		if (__current_playitem_start != undefined) {
			var video_time = __current_playitem_start.getTime() + (time * 1000.0) + (camera_timezone_ms + local_timezone_offset_ms); // (vlc.input.position * current_playitem_duration);
			//console.log("position changed", video_time, "started:", __current_playitem_start, "elapsed:", time * 1000.0);
			main.demo.moveCursor(video_time);
		} else {
			console.log("current_playitem_start == null")
		}
	}
}
function irisplayer_state_change(state) {
	console.log("state", state);
	if (___video_stopped == true) return;
	if (__is_live_stream == true) {
		if (state == "loading" || state == "ready" || state == "playing") {
			camerasView.selected().is_live(true);
		} else {
			camerasView.selected().is_live(false);
		}
		if (state == "loading") {
			notif({type: "success", msg: "Starting live stream", position: "center", opacity: 0.8});
		}
	} else {
		// how to show user that recording is playing or not ? she will see the video
	}
	if (state == "playbackError") {
		var key = CookieUtils.readCookie("key");
		$.get(DB_SERVER_URL + '/session_expired?key=' + key, function(data){
			if (! JSON.parse(data)['result']) {
				notif({type: "error", msg: "Session has Expired. Please login again", position: "center", opacity: 0.8});
				logout();
			} else {
				notif({type: "error", msg: "Camera is unreachable", position: "center", opacity: 0.8});
				position_to_last_recording(false);				
			}
		});
	}
}

function launchIntoFullscreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}

function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

function irisplayer_fullscreen() {

	if (!__irisplayer_fullscreen) {
		launchIntoFullscreen(document.getElementById("_player_container"));
		__irisplayer_fullscreen = true;
	} else {
		exitFullscreen();
		__irisplayer_fullscreen = false;
	}
	console.log("Flash Player fullscreen: " + __irisplayer_fullscreen);
}

function flash_log(txt) {
	console.log("[flash]", txt)
}

function _stop_player() {
	$("#camera-download-button").addClass("disabled");
	var player = document.getElementById("IrisPlayer");
	player.stop();
	camerasView.selected().is_live(false);
	___video_stopped = true;
}
// Designed for single flash player element on the page
function play(url, current_playitem_start, current_playitem_duration, is_live_stream) {
	__is_live_stream = is_live_stream;
	__current_playitem_start = current_playitem_start;
	__current_playitem_duration = current_playitem_duration;
	// flash player logic:
	console.log("FLASH play", url);
	
	if (url.length > 0 && url.search("live") < 0) {
		$("#camera-download-button").removeClass("disabled");
		//url_current_download =  url;
		$("#camera-download-button").attr("target", "_blank");
		$("#camera-download-button").attr("href", url);
	} 

	var player = document.getElementById("IrisPlayer");
	___video_stopped = false;
	//url = checkUrl(url);

	player.play(url);
}

function checkUrl(url) {

	var key = CookieUtils.readCookie("key");
	if (key == undefined || key == "undefined") {
		key = test_key;
		console.log("COOKIE KEY UNAVAILABLE => SET KEY TO: ", key);
	}
	if(url === undefined){
		url = url_live;
		url = url.replace("{{KEY}}", key);
		var selected_camera = camerasView.selected();
		if (selected_camera.state.external_ip() == IP) {
			console.log("browser is in the same LAN as camera");
			notif({type: "success", msg: "Streaming within LAN", position: "center", opacity: 0.8});
			url = url.replace("{{IP}}", selected_camera.state.internal_ip());
			url = url.replace("{{PORT}}", selected_camera.state.internal_port());
		} else {
			console.log("browser is NOT in the same LAN as camera");
			notif({type: "success", msg: "Streaming outside LAN", position: "center", opacity: 0.8});
			url = url.replace("{{IP}}", selected_camera.state.external_ip());
			url = url.replace("{{PORT}}", selected_camera.state.external_port());
		}
	}else{
		url += "&key=" + key;
	}
	return url;
};

function place_player() {
	var swfVersionStr = "0";
	var xiSwfUrlStr = "";
	var flashvars = {};
	var params = {};
	params.quality = "high";
	params.bgcolor = "#ffffff";
	params.allowscriptaccess = "always";
	params.wmode = "opaque";
	var attributes = {};
	attributes.id = "IrisPlayer";
	attributes.name = "IrisPlayer";
	attributes.class= "IrisPlayer";
	attributes.align = "middle";
	swfobject.embedSWF(
		"IrisPlayer.swf", "_player_video",
		"100%", "100%",
		swfVersionStr, xiSwfUrlStr,
		flashvars, params, attributes
	);
}

function user_on_load() {

	var PlayerSize = {width: "720", height: "480"};

	$(window).resize(function () {
		var top_margin = 190;

		var new_width = $($(".container_video")[0]).width();
		//var player = document.getElementById("_player_container");
		/*
		if (PlayerSize.width !== new_width) {
			//player.width = new_width + "px";
			PlayerSize.width = new_width;
		}
		*/
		
		/*
		var new_height = $('body').height() - top_margin;
		new_height = new_height > 480 ? 480 : new_height;
		*/
		var new_height = new_width / 1280 * 720 + 64;

		console.log(new_width, new_height);

		/*
		if (PlayerSize.height !== new_height) {
			//player.height = new_height + "px";
			PlayerSize.height = new_height;
		}
		*/
		
		$("#_player_container").css("width", new_width + "px");
		$("#_player_container").css("height", new_height + "px");
	});
	$(window).resize();

	try {
		console.log(" > placing player ..");
		place_player();
	} catch (e) {
		console.log(e)
	}

	var key = CookieUtils.readCookie("key");
	console.log("key", key, typeof(key));
	if (key == undefined || key == "undefined") {

		//not logged in
		window.location = "/";
		valid_login();

	} else {
        $.get(DB_SERVER_URL + '/session_expired?key=' + key, function(data){
        	if (JSON.parse(data)['result']) {
        		valid_login();
        	} else {
	            setTimeout(function(){
	        		notif({type: "error", msg: "Session has Expired. Please login again", position: "center", opacity: 0.8});
	        	}, 300);
        		logout();
        	}
		});
    }
}

function valid_login() {
	console.log("valid, proceed");
	main = new Main();
	main.init_user();
}

function logout(e) {
	//e.preventDefault();
	var key = CookieUtils.readCookie("key");
	$.get(DB_SERVER_URL + '/client_logged_out?key=' + key, function(data){
		console.log(data, "logout request");
		var result = CookieUtils.eraseCookie("key");
		var key = CookieUtils.readCookie("key", "/");
		console.log("key exists ?", key, result);
		window.location = "/"
	});	
}

// unused
function server_time_to_camera_timezone(seconds) {
	var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
	console.log("local_timezone_offset_ms===>", local_timezone_offset_ms);
	var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;
	console.log("camerasView.selected().state.camera_timezone_time_diff()===>", camerasView.selected().state.camera_timezone_time_diff());
	console.log("camera_timezone_ms===>", camera_timezone_ms);
	var ret = (seconds * 1000) + local_timezone_offset_ms + camera_timezone_ms;
	console.log("ret===>", ret);
	//console.log("server_time_to_camera_timezone loc=", local_timezone_offset_ms, "cam=", camera_timezone_ms, "res=", ret);
	return ret;
}

function secondToDateString(seconds) {
	var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
	var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;
	var ret = (seconds * 1000) + local_timezone_offset_ms + camera_timezone_ms;

	var date = new Date(ret);

	var years = date.getFullYear();
	var months = date.getMonth() + 1;
	var days = date.getDate();
	/*
	var hours = date.getHours();
	var minutes = "0" + date.getMinutes();
	var seconds = "0" + date.getSeconds();
	*/

	//var formattedDate = years + "/" + months + "/" + days + " " + hours + ':' + minutes.substr(minutes.length-2) + ':' + seconds.substr(seconds.length-2);

	var formattedDate = months + "/" + days + "/" + years;

	return formattedDate;
}

/**
 * Main is object that holds logic for whole page
 * @constructor
 */
var Main = function () {
	var _this = this;
	_this.init_finished = false;

	// DRAW EMPTY TIMELINE
	var end = Date.now() + (1000 * 60 * 5);
	var start = end - (1000 * 60 * 10);

	_this.camera_init = {
		get_data_for_camera: false,
		get_general_camera_params: false
	};

	_this.init_user = function () {
		_this.demo = new Demo(_this.change_video_src);
		Communication.get_user_cameras(_this.user_data_ready, logout, false);
	};

	_this.user_data_ready = function (data) {
		var cameras = data['cameras'];
//		__my_ip_address = data.your_ip_address;
//		__subscription_start_time = data.subscription_start_time * 1000.0;
//		__subscription_end_time = data.subscription_end_time * 1000.0;
//		__subscription_camera_count = cameras.length;
		console.log("SUB", __subscription_start_time, __subscription_end_time, "data", data);
		var sub_start = data.subscription_start_time !== undefined && data.subscription_start_time !== null
			? secondToDateString(data.subscription_start_time) : "Not subscribed";
		var sub_end = data.subscription_end_time !== undefined && data.subscription_end_time !== null
			? secondToDateString(data.subscription_end_time) : "Not subscribed";
		camerasView.subscription_start_time(sub_start);
		camerasView.subscription_end_time(sub_end);

		user_email = data.user_email === undefined || data.user_email === null ? "Not available" : data.user_email;
		camerasView.user_email(user_email);
		camerasView.phone_number(data.phone_number);
		camerasView.telephone_co(data.telephone_co);

		IP = data["your_ip_address"];
		console.log("IP: ", IP);
		camerasView.cameras([]);
		$.each(cameras, function (index, camera) {
			var camera_data = new CameraData(camera);
			camerasView.cameras.push(camera_data);
			console.log("mac address:", camera_data.mac_address, data);
			_this.camera_init_count++;
			_this.get_camera_data(camera.id, false);
		});
		if (camerasView.cameras().length > 0) {
			_this.camera_init.get_data_for_camera = false;
			_this.camera_init.get_general_camera_params = false;
			_this.init_finished = false;
			camerasView.selected(camerasView.cameras()[0]);
			_this.set_selected_camera_shadow();
		}

		//fillin_camera_subscription();
		var date2 = new Date(sub_end);
		var date1 = new Date();
		var num_days = calcDataDiffDays(date1.toString(), date2.toString());
		/*console.log("Date 2 orig : "+sub_end);
		console.log("Date 1 :"+date1.toString());
		console.log("Date 2 :"+date2.toString());

		console.log("Num of Days : "+num_days);*/

		if (num_days > 30) {
			$("#subscription-warn-message").hide();
		}
	};

	_this.select_camera = function (camera_id) {
		var camera = _this.get_camera(camera_id);
		if (camera !== undefined) {
			camerasView.selected(camera);
			_this.set_selected_camera_shadow();
			//ko.applyBindings(camerasView.selected()); //  TODO: kakoovo uraditi
		}
	};

	_this.get_camera_data = function (camera_id, show_notify) {
		Communication.get_data_for_camera(camera_id, _this.camera_data_ready, show_notify);
		Communication.get_general_camera_params(camera_id, _this.general_data_ready, show_notify);
	};

	_this.camera_data_ready = function (data, camera_id) {
		var camera_segments = data['segments'];
		console.log("camera_data_ready", camera_segments, camera_id);
		var camera = _this.get_camera(camera_id);
		if (camera === undefined) {
			return;
		}
		camera.segments(camera_segments);

		if (camerasView.selected().id() === camera_id) {
			_this.camera_init.get_data_for_camera = true;
			if (_this.camera_init.get_general_camera_params) {
				_this.init_finished = true;
			}

			var place_segment_function = null;

			place_segment_function = function () {
				if (__camera_state_fetched[camera.id()] == undefined) {//camera.state.camera_timezone == undefined) {
					// state have't ajaxed yet
					console.log("waiting for state ...");
					setTimeout(place_segment_function, 500);
					return;
				}
				console.log("state is ready ...");
				var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
				var camera_timezone_ms = parseFloat(camera.state.camera_timezone_time_diff() || 0) * 3600000;
				var events = [];
				$.each(camera_segments, function (index, item) {
					var start = (item.utc_start * 1000) + local_timezone_offset_ms + camera_timezone_ms;
					var end = (item.duration * 1000) + start;
					events.push({
						id: index,
						content: "",
						start: new Date(start),
						end: new Date(end),
						className: ""
					});
				});
				console.log('start !!!');

				if (camera_segments.length > 0) {
					// select last recoroding not the first one !
					var cur = server_time_to_camera_timezone(camera_segments[0].utc_start);
					console.log(" ! moveCursor place_segment_function", cur);
					_this.demo.moveCursor(cur);
					// _this.demo.moveCursor(camera_segments[camera_segments.length -1].utc_start * 1000);
					_this.demo.addMotionEvents(events);
					//var recordings = main.demo.itimeline.timeline.getEventData("recordings");
					//console.log("cursor positioned", camera_segments[0].utc_start * 1000, recordings[0]);
				} else {
					var now_in_camera_timezone = (new Date()).getTime() + local_timezone_offset_ms + camera_timezone_ms;
					console.log("placing cursor on current time, no recordings :)", now_in_camera_timezone);
					main.demo.moveCursor(now_in_camera_timezone);
				}
			}

			place_segment_function();
		}
	};

	_this.change_video_src = function (time_) {
		camerasView.selected().is_live(false);
		// convert time into utc
		var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
		var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;

		// return (seconds * 1000) + local_timezone_offset_ms + camera_timezone_ms;
		// convert back to database time

		var time = new Date(time_.getTime() - (camera_timezone_ms + local_timezone_offset_ms));
		console.log("Corrected", time);
		console.log("original ", time_);

		var stage_len = main.demo.timeline.GetStageTimeLength(); //TODO
		var magnet_range = stage_len * 0.01;
		var segments = camerasView.selected().segments();
		var closest_segment = undefined;
		for (var i = 0; i < segments.length; i++) {
			var item = segments[i];
			var duration = item.duration * 1000;
			var start = item.utc_start * 1000;
			var end = start + duration;
			var s = new Date(start);
			var e = new Date(end);
			if (time > s && time < e) {
				// found that segment
				_this._current_playitem_start = s;
				_this._current_playitem_duration = duration;
				play(checkUrl(item.video_url), _this._current_playitem_start, _this._current_playitem_duration, false);
				return;
			} else {
				// determine closest segment in treshold
				var distance = Math.min(
					Math.abs(s - time),
					Math.abs(e - time)
				)
				//console.log("# distance=", distance, "magnet=", magnet_range, "stage=", stage_len);
				if (distance < magnet_range) {
					// within treshold, is it closest so far ?
					if (closest_segment == undefined || distance < closest_segment.distance) {
						//console.log("# near", distance, s);
						closest_segment = {video_url: item.video_url, distance: distance, start: s, duration: duration}
					} else {
						//console.log("# far", distance, s)
					}
				}
			}
		}
		if (closest_segment != undefined) {
			_this._current_playitem_start = closest_segment.start;
			_this._current_playitem_duration = closest_segment.duration;
			play(checkUrl(closest_segment.video_url), _this._current_playitem_start, _this._current_playitem_duration, false);
			return;
		}
		_stop_player();
	};

	_this.general_data_ready = function (data, camera_id) {
		general_data = data['state'];
		var camera = _this.get_camera(camera_id);
		if (camera === undefined) {
			console.log('general_data_ready => camera not found');
			return;
		}
		camera.state.external_ip(general_data.external_ip);
		camera.state.external_port(general_data.external_port);
		camera.state.internal_ip(general_data.internal_ip);
		camera.state.internal_port(general_data.internal_port);
		camera.state.microphone(toBool(general_data.microphone));
		camera.state.motion_sensor(toBool(general_data.motion_sensor));
		camera.state.night_vision(toBool(general_data.night_vision));
		camera.state.online(toBool(general_data.online));
		camera.state.recording_mode(toBool(general_data.recording_mode));
		camera.state.recording_quality(general_data.recording_quality === "HD");
		camera.state.speaker(toBool(general_data.speaker));

		camera.state.device_title(general_data.device_title);
		camera._name(general_data.device_title);
		camera.state.device_address(general_data.device_address || "Title");
		// camera.state.device_note(general_data.device_note);
		camera.state.camera_timezone_time_diff(general_data.camera_timezone_time_diff || 0);
		camera.state.camera_timezone_local_area(general_data.camera_timezone_local_area);
		camera.state.wakeup_auto_monitor(toBool(general_data.wakeup_auto_monitor));
		
		__camera_state_fetched[camera_id] = true;

		//fillin_camera_settings();

		// start live
		if (___camera_ready == false && ___flash_ready == true) {
			// flash is already loaded
			console.log("start live stream");
			main.go_live();
		}

		if (camerasView.selected().id() === camera_id) {
			_this.camera_init.get_general_camera_params = true;
			if (_this.camera_init.get_data_for_camera) {
				_this.init_finished = true;
			}
		}
		___camera_ready = true;
	};

	_this.send_general_data = function (show_notify) {
		var curr_rec_val = camerasView.selected().state.recording_quality();
		//ARRAY OF DAYS TO STRING - BECAUSE SERVER CAN STORE ONLY STRINGS AND NUMBERS
		var curr_days_cal = camerasView.selected().state.schedule_by_days();
		var val = curr_days_cal.join();

		var state_json = ko.toJSON(camerasView.selected().state);
		state_json = state_json.replace(/:true/g, ':"true"');
		state_json = state_json.replace(/:false/g, ':"false"');
		state_json = state_json.replace('"recording_quality":"true"', '"recording_quality":"HD"');
		state_json = state_json.replace('"recording_quality":"false"', '"recording_quality":"LD"');
		state_json = state_json.replace(/"schedule_by_days".*],"email_alert_active/, '"schedule_by_days":"' + val + '", "email_alert_active');

		var jsonData = {'json_state': state_json};
		console.log("jsonData => ", jsonData);
		camerasView.selected().state.recording_quality(curr_rec_val);
		Communication.set_general_camera_params(camerasView.selected().id(), jsonData, show_notify);
	};

	_this.send_account_data = function (show_notify) {

		var phone_number = camerasView.phone_number();
		var telephone_co = camerasView.telephone_co();

		var state_json = "{" + 
			"\"phone_number\": \"" + phone_number + "\"" + "," +
			"\"telephone_co\": \"" + telephone_co + "\"" +
			"}";
		

		var jsonData = {'json_state': state_json};
		console.log("jsonData => ", jsonData);

		Communication.set_general_account_params(jsonData, show_notify);
	};


	_this.get_camera = function (camera_id) {
		var target_camera = undefined;
		if (camera_id !== undefined) {
			$.each(camerasView.cameras(), function (index, camera) {
				if (camera.id() === camera_id) {
					target_camera = camera;
				}
			});
		}
		return target_camera;
	};

	_this.set_camera = function (camera) {
		_this.init_finished = false;
		camerasView.selected(camera);
		_this.init_finished = true;
		console.log("new camera selected", camerasView.selected());

		// SET MOTION EVENTS OF NEW CAMERA
		_this.demo.removeMotionEvents();
		
		/*
		var events = [];
		$.each(camerasView.selected().segments(), function (index, item) {
			var ff = item.utc_start * 1000;
			var dd = (item.duration * 1000);
			events.push({start: new Date(ff), end: new Date(ff + dd)});
		});
		*/

		// fill in form data:
		//fillin_camera_settings();
		
		
		var place_segment_function = null;
		var camera_segments = camerasView.selected().segments();

		place_segment_function = function () {
			if (__camera_state_fetched[camera.id()] == undefined) {//camera.state.camera_timezone == undefined) {
				// state have't ajaxed yet
				console.log("waiting for state ...");
				setTimeout(place_segment_function, 500);
				return;
			}
			console.log("state is ready ...");
			var local_timezone_offset_ms = (new Date()).getTimezoneOffset() * 60000;
			var camera_timezone_ms = parseFloat(camera.state.camera_timezone_time_diff() || 0) * 3600000;
			var events = [];
			$.each(camera_segments, function (index, item) {
				var start = (item.utc_start * 1000) + local_timezone_offset_ms + camera_timezone_ms;
				var end = (item.duration * 1000) + start;
				events.push({
					id: index,
					content: "",
					start: new Date(start),
					end: new Date(end),
					className: ""
				});
			});
			console.log('start !!!');

			if (camera_segments.length > 0) {
				// select last recoroding not the first one !
				var cur = server_time_to_camera_timezone(camera_segments[0].utc_start);
				console.log(" ! moveCursor place_segment_function", cur);
				_this.demo.moveCursor(cur);
				_this.demo.addMotionEvents(events);
			} else {
				var now_in_camera_timezone = (new Date()).getTime() + local_timezone_offset_ms + camera_timezone_ms;
				console.log("placing cursor on current time, no recordings :)", now_in_camera_timezone);
				main.demo.moveCursor(now_in_camera_timezone);
			}
		}

		place_segment_function();

	
		_this.set_selected_camera_shadow();
		_this.go_live();
	};

	_this.latest_thumbnail_from_camera = function (camera) {
		var url = "#";
		if (camera == undefined) {
			console.log("[E] _latest_thumbnail_from_camera camera == undefined", camera.mac_address());
			return url;
		}

		var segments = camera.segments();
		for (var i = segments.length - 1; i >= 0; i--) {
			var segment = segments [i];
			if ((segment != undefined) && (segment.thumbnail_url != undefined) && (segment.thumbnail_url !== "")) {
				var key = CookieUtils.readCookie("key");
				key = key ? key : test_key
				url = segment.thumbnail_url + "&key=" + key;
			}
		}
		console.log(" ===> url ", url, camera.mac_address());
		return url;
	};
	
	
	_this.get_camera_zone_time = function() {
		var now = new Date();
		var local_timezone_offset_ms = now.getTimezoneOffset() * 60000;
		var camera_timezone_ms = parseFloat(camerasView.selected().state.camera_timezone_time_diff() || 0) * 3600000;
		var now_in_camera_timezone = now.getTime() + local_timezone_offset_ms + camera_timezone_ms;

		return now_in_camera_timezone;
	}

	_this.go_live = function () {
		//console.log("GO LIVE", Date.now());
		console.log("GO LIVE", _this.get_camera_zone_time());
		setTimeout(function () {
			//_this.demo.MoveWindowToCursor(Date.now());
			_this.demo.MoveWindowToCursor(_this.get_camera_zone_time());
		}, 300);

		//_this.demo.moveCursor(Date.now());
		_this.demo.moveCursor(_this.get_camera_zone_time());

		play(checkUrl(), null, null, true);
	};

	_this.test = function () {
		var camera_id = camerasView.selected().id();
		Communication.get_camera_settings(camera_id, _this.test_callback, true);
	};

	_this.test_callback = function (data) {
		console.log("vratio: ", data);
	};

	_this.set_selected_camera_shadow = function () {
		var camera = camerasView.selected();
		$.each($('.list_item'), function (index, item) {
			var a = ko.dataFor(item);
			var cam = $(item);
			if (a === camera) {
				cam.addClass('selected_camera');
				cam.find(".thumbnail.thumb_style").css("box-shadow","1px 2px 8px 0 rgba(0, 63, 158, 1)");
			} else {
				cam.removeClass('selected_camera');
				cam.find(".thumbnail.thumb_style").css("box-shadow","none");
			}
		});
	};

	_this.test_sms = function(show_notify) {
		var phone_number = camerasView.phone_number();
		var telephone_co = camerasView.telephone_co();

		var state_json = "{" + 
			"\"phone_number\": \"" + phone_number + "\"" + "," +
			"\"telephone_co\": \"" + telephone_co + "\"" +
			"}";

		var jsonData = {'json_state': state_json};
		console.log("jsonData => ", jsonData);
		notif({type: "success", msg: "SMS test message sent.", position: "center", opacity: 0.8});
		Communication.send_test_sms(jsonData, show_notify);
	};
};

function save_general(e) {
	console.log("main.init_finished", main.init_finished);
	if (main.init_finished) {
		console.log("go to save");
		main.send_general_data(false);
	}
}

function save_account(e) {
	console.log("main.init_finished", main.init_finished);

	console.log("go to save account");
	main.send_account_data(false);
}

function on_off_cam() {
	if (main.init_finished) {
		Communication.set_camera_on_off(camerasView.selected().id(), camerasView.selected().state.online(), true);
		save_general();
	}
}

function on_off_rec() {
	if (main.init_finished) {
		camerasView.selected().toggle_recording();
		save_general();
	}
};

var thumb_url = function(camera){
	var url = "http://{{IP}}:{{PORT}}/thumbnail/{{KEY}}";
	var key = CookieUtils.readCookie("key");
	if (key == undefined || key == "undefined") {
		key = test_key;
		console.log("COOKIE KEY UNAVAILABLE => SET KEY TO: ", key, camera.mac_address());
	}
	url = url.replace("{{KEY}}", key);
	if (camera.state.external_ip() == IP) {
		console.log("browser is in the same LAN as camera", camera.mac_address());
		notif({type: "success", msg: "Streaming within LAN", position: "center", opacity: 0.8});
		url = url.replace("{{IP}}", camera.state.internal_ip());
		url = url.replace("{{PORT}}", camera.state.internal_port());
	} else {
		console.log("browser is NOT in the same LAN as camera", camera.mac_address());
		notif({type: "success", msg: "Streaming outside LAN", position: "center", opacity: 0.8});
		url = url.replace("{{IP}}", camera.state.external_ip());
		url = url.replace("{{PORT}}", camera.state.external_port());
	}

	console.log("thumb_url", url, camera.mac_address());
	return url;
};


function camera_tabs() {
 	var key = CookieUtils.readCookie("key");
 	var camera_counter = 1;
 	console.log(key);
 	$.get(DB_SERVER_URL+'/user_cameras/?key='+key, function(data){
		var json_data = $.parseJSON(data);
		if (json_data.result) {
			$.each(json_data.cameras, function(key, val) {
				var set_active = "";
				var tab_active = "";
				var camera_name = json_data.cameras[key].state.device_title;
				if (camera_name.length <= 0) {
					camera_name = "Camera "+key;
				}
				if (camera_counter == 1) {
					set_active = "class=\"active\"";
					tab_active = "active";
				}
				$(".camera-tabs").append("<li role=\"presentation\" "+set_active+"><a href=\"#camera-"+key+"\" data-toggle=\"tab\" id=\"cameratab-"+key+"\">"+camera_name+"</a></li>");//.html(json_data.cameras[key].state.device_address);*/

				if (camera_counter == 1) {
					$("#camera-name").html(camera_name);
					current_cam_json_state = json_data.cameras[key];
				}
				/*var cam_tabs_cont = $("#cameraTabContent");
				cam_tabs_cont.append("<div class=\"tab-pane fade in "+tab_active+"\" id=\"camera-"+key+"\"></div>");
				cam_tabs_cont.find("#camera-"+key).append("<div class=\"tab-top-content float-wrap\"></div>");
				cam_tabs_cont.find("#camera-"+key).find(".tab-top-content").append("<input type=\"checkbox\" name=\"camera-on\" class=\"camera-on-button\"> <span>"+camera_name+"</span>");
				cam_tabs_cont.find("#camera-"+key).find(".tab-top-content").append("<span class=\"camera-right-button\">"
                                    +    "<button type=\"button\" class=\"btn btn-default btn-sm\" aria-label=\"Left Align\">"
                                    +    "    <span class=\"glyphicon glyphicon-download-alt\" aria-hidden=\"true\"></span>"
                                    +    "    Download recording"
                                    +    "</button> "
                                    +    "<button type=\"button\" class=\"btn btn-default btn-sm btn-red\" aria-label=\"Left Align\">"
                                    +    "    <span class=\"glyphicon glyphicon-record\" aria-hidden=\"true\"></span>"
                                    +    "</button>"
                                    + "</span>");
				cam_tabs_cont.find("#camera-"+key).append("<div class=\"chk_cam_player_video\" ></div>");

				if (camera_counter == 1) {

					cam_tabs_cont.find("#camera-"+key).find(".chk_cam_player_video").append("<div class=\"container_video\">" +
													    "<div id=\"_player_image\">" +
													    "</div>" +
													    "<div id=\"_player_container\">" +
													    "    <div id=\"_player_video\" class=\"player\">" +
													    "        <img class=\"img-responsive\" src=\"img/img_for_v_2/test_cam_pic.jpg\">" +
													    "    </div>" +
													    "</div>" +
													    "<div class=\"controls\">" +
													    "    <div class=\"timeline_holder\">" +
													    "        <div style=\"float: left;border-bottom-left-radius: 4px;\" class=\"time_bicon\">" +
													    "            <p id=\"left_time\" class=\"time_bicon_p\"></p>" +
													    "        </div>" +
													    "        <div id=\"demo_timeline\" class=\"demo_timeline\"></div>" +
													    "        <div style=\"float: right;border-bottom-right-radius: 4px;\" class=\"time_bicon\">" +
													    "            <p id=\"right_time\" class=\"time_bicon_p\"></p>" +
													    "        </div>" +
													    "        <div class=\"time_links_holder\">" +
													    "            <div class=\"stage_change_holder\"><p class=\"stage_change selected\">10min</p></div>" +
													    "            <div class=\"stage_change_holder\"><p class=\"stage_change\">1h</p></div>" +
													    "            <div class=\"stage_change_holder\"><p class=\"stage_change\">24h</p></div>" +
													    "            <div class=\"stage_change_holder\"><p class=\"stage_change\">7d</p></div>" +
													    "            <div class=\"stage_change_holder\"><p class=\"stage_change\">1mon</p></div>" +
													    "            <div class=\"stage_change_holder\" style=\"float: right\"" +
													    "                 data-bind=\"style: { display: camerasView.selected().is_live() ? 'none' : 'block' }, click: function(){camerasView.selected().is_live(true); main.go_live()}\">" +
													    "                <p class=\"go_live\"> Go live </p>" +
													    "            </div>" +
													    "            <div class=\"stage_change_holder\" style=\"float: right\"" +
													    "                 data-bind=\"style: { display: camerasView.selected().is_live() ? 'block' : 'none' }, click: function(){switch_to_recordings()}\">" +
													    "                <!-- <p class=\"live\" >Recordings</p> -->" +
													    "                <p class=\"go_live\">Stop live</p>" +
													    "            </div>" +
													    "        </div>" +
													    "    </div>" +
													    "</div>" +
														"</div>");
			}*/
				camera_counter++;
			});

			camerasView = new CamerasView();
			ko.applyBindings(camerasView);
			
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				var currentTab = $(e.target).text();
				var css_id = $(this).attr("id");
				var id = css_id.replace("cameratab-","");

				$("#camera-download-button").addClass("disabled");
				$("#camera-name").html(currentTab);

				current_cam_json_state = json_data.cameras[id];

				main.set_camera(camerasView.cameras()[id]);
			});			

			$(".camera-on-button").bootstrapSwitch();
		} else {
			CookieUtils.eraseCookie("key");
			window.location = "/"
		}

	});	
}

function downloadURI(uri, name) {
  var link = document.createElement("a");
  link.download = name;
  link.href = uri;
  link.click();
}

function calcDataDiffDays(date1, date2) {
	var d_date1 = Date.parse(date1);
	var d_date2 = Date.parse(date2);
	//var d_date2 = date2;

	var minutes = 1000 * 60;
    var hours = minutes * 60;
    var days = hours * 24;

	var days = Math.round((d_date2 - d_date1) / days);

	return days;
}

function loadCamData(json_data) {

}

function saveCamData(json_data) {

}