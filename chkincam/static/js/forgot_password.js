function forgot_password(e) {
    e.preventDefault();
    $('#ForgotForm').bootstrapValidator('validate');

    var email = $("#email").val();
    var captcha_response = $(".g-recaptcha-response").val();
    var valid = true;
    if(!email){
        valid = false;
        return email_invalid();
    }
    if(!captcha_response){
        valid = false;
        return captcha_failed();
    }
    //notify the user that the request is in progress
    if (valid){
    request_in_progress();
    $.ajax({type: "POST", url: "/forgot_password", dataType: "json",
        data: {
            email: email,
            captcha_response: captcha_response
        },
        success: function (data, textStatus, jqXHR) {
            if (data.error_code == 0) {
                return success_notify();
            } else if (data.error_code == 14) {
                return captcha_failed();
            } else if (data.error_code == 1) {
                return email_invalid();
            } else {
                return server_unavailable();
            }
        }
    }).fail(function() {
        server_unavailable();
        });
    }
}

function email_invalid(){
    notif({
        type: "error",
        msg: "Please check the entered Email",
        position: "center",
        opacity: 0.8
    });
}

function success_notify(){
    notif({
        type: "success",
        msg: "Password is sent to your inbox",
        position: "center",
        opacity: 0.8
    });    
}

function server_unavailable(){
    notif({
        type: "error",
        msg: "Server is unavailable, please try again later",
        position: "center",
        opacity: 0.8
    });
}

function captcha_failed() {
    notif({
        type: "error",
        msg: "Captcha verification failed. Try again",
        position: "center",
        opacity: 0.8
    });    
}

function request_in_progress() {
    notif({
        type: "info",
        msg: "Request is in progress. Please wait",
        position: "center",
        opacity: 0.8
    });       
}