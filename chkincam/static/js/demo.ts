/// <reference path="../libs/jquery/jquery.d.ts" />
/// <reference path="../libs/timeline/beeTimeline.ts" />

class Demo {
	private timeline:BTL;
	private last_left_time:string;
	private last_right_time:string;
	private left_bicom = $('#left_time');
	private right_bicom = $('#right_time');
	private change_src_callback:(n:Date) => any;

	constructor(change_src_callback:(n:Date) => any) {
		this.change_src_callback = change_src_callback;
		var el:HTMLElement = document.getElementById('demo_timeline');
		this.timeline = new BTL(el, new TimeSpan(SpanType.MINUTE, 10));
		this.setBiconsTime();
		this.checkTimeBiconTextPosition();
		this.checkTimeBiconTextPosition();
		this.registerEvents();
	}

	public addMotionEvents(events:beeItem[]):void {
		this.timeline.AddItems(events);
	}

	public removeMotionEvents():void {
		// TODO: IMPLEMENT THIS
		this.timeline.RemoveItems();
	}

	public moveCursor(video_time:number):void {
		if (isNaN(video_time)) {
			return;
		}
		this.timeline.SetCursorTime(new Date(video_time));
	}

	private setBiconsTime():void {
		var time = this.timeline.GetTimeBorders();
		var left = moment(time[0]).format("MMM D");
		var right = moment(time[1]).format("MMM D");

		if (this.last_left_time !== left) {
			this.last_left_time = left;
			this.left_bicom.text(left);
		}
		if (this.last_right_time !== right) {
			this.last_right_time = right;
			this.right_bicom.text(right);
		}
	}

	private checkTimeBiconTextPosition():void {
		var w = $(window).width();
		var margin_val = 12;
		if (w < 664) {
			margin_val = 2;
		}
		$('.time_bicon_p').css('margin-top', margin_val + 'px');
	}

	private registerEvents() {
		$(document).on("cursorChange", (e) => {
			var data = e["time"];
			console.log("cursorChange", e, data);
			this.change_src_callback(data);
		});

		$('.stage_change').on("click", (e:any) => {
			var val = e.target.innerHTML;
			if (val === "10min") {
				this.timeline.SetTimeSpan(SpanType.MINUTE, 10);
			} else if (val === "1h") {
				this.timeline.SetTimeSpan(SpanType.HOUR, 1);
			} else if (val === "24h") {
				this.timeline.SetTimeSpan(SpanType.HOUR, 24);
			} else if (val === "7d") {
				this.timeline.SetTimeSpan(SpanType.DAY, 7);
			} else if (val === "1mon") {
				this.timeline.SetTimeSpan(SpanType.MONTH, 1);
			}
			$(".stage_change").removeClass("selected");
			$(e.target).addClass("selected");
			this.setBiconsTime();
		});
		jQuery(document).on('timelineTimeChange', (e) => {
			this.setBiconsTime();
		});

		$(window).on('resize', (e:any) => {
			this.checkTimeBiconTextPosition();
		});
	}

	public MoveWindowToCursor(time: Date): void{
		this.timeline.MoveWindowToCursor(time);
	}
}