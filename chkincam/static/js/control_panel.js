
var dataSet = [
    ['Alexandar Kordic','alexandarkordic@gmail.com','<input type="checkbox" name="vehicle"> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Jon Sarah','jon_s@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Peter Floe','p_floe12@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Maxim Lock','maxx_g@gmail.com','<input type="checkbox" name="vehicle"> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Kates Flow','kate_fm@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Bobs','bobi00@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['ThomasK','thomas.k@gmail.com','<input type="checkbox" name="vehicle"> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['JensZ','jenazz@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Susan Oblenko','susan_obl@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['AmandaBob','amanda002@gmail.com','<input type="checkbox" name="vehicle"> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Alex1','alex666@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Samantha Sims','samanta325@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Dave','the_dave@gmail.com','<input type="checkbox" name="vehicle"> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Halle Formando','formando1@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">'],
    ['Fred Slow','fredy@gmail.com','<input type="checkbox" name="vehicle" checked> Is active','<input type="checkbox" name="vehicle" class="delete_check">']
];

var camera_template = "<div class='cam_container'><div class='cam_name'><p><b>{{CAM_NAME}}</b></p></div><div class='empty_cam'></div><button type='button' class='btn btn-danger btn-xs btn_del_cam'>Delete camera</button></div>";

$(document).ready(function() {

    // NAVIGATION
    $('#fullpage').fullpage({
        slidesNavigation: false,
        autoScrolling: false,
        verticalCentered: false
    });

    $('#back_btn').on('click', function(event){
        $.fn.fullpage.moveSlideLeft();
    });


    // ADD USER
    $('#add_user').on('click', function(event){
        event.preventDefault();
        var username = $('#user_name').val();
        var email = $('#email').val();

        $('#example').dataTable().fnAddData( [
                username,
                email,
                '<input type="checkbox" name="vehicle"> Is active',
                '<input type="checkbox" name="vehicle" class="delete_check">'
            ]
        );
    });


    // DELETE USER
    $("#delete_users").on('click', function(event){
        var selected = $( "input[class='delete_check']:checked" );
        $.each(selected, function(index, item){
            var tr = $(item).closest('tr')[0];

            var aPos = table.fnGetPosition(tr);
            table.fnDeleteRow(aPos);
        });

        //table.row('.selected').remove().draw( false );
    });

    // ADD CAM
    $('#add_camera').on('click', function(event){
        event.preventDefault();
        var camName = $('#cam_name_field').val();
        $('#cam_name_field').val('');
        addCam(camName);
    });


    // DELETE CAM
    $(".btn_del_cam").on('click', function(event){
        var camera = event.target.parentElement;
        removeCam(camera);
    });


    // RENDER TABLE
    var table = $('#example').dataTable( {
        "data": dataSet,
        "columns": [
            { "title": "User" },
            { "title": "Email" },
            { "title": "Status" },
            { "title": "Delete"}
        ],
        "aoColumns": [
            { "sWidth": "40%" },
            { "sWidth": "50%", "bSortable": false },
            { "sWidth": "8%", "bSortable": false  },
            { "sWidth": "2%", "bSortable": false }
        ]
    } );


    // USER SEECTION
    $('#example tbody').on( 'click', 'tr', function (e) {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        if($(e.target).hasClass('sorting_1')){
            $.fn.fullpage.moveSlideRight();
        }
    } );
} );

var addCam = function(name){
    if(name === ""){notify('Please enter the name of camera', 'error'); return;}
    var camera = camera_template.replace("{{CAM_NAME}}", name);
    camera = $(camera);
    $('.camera_list').append(camera);
    var b = camera.find( "button" );
    b.on('click', function(e){
        removeCam(camera);
    });
}

var removeCam = function(camera){
    camera.remove();
}

function notify(msg, type) {
    notif({
        type: type,
        msg: msg,
        position: "center",
        opacity: 0.8
    });
}