Player = function(){
    var _this = this;
    _this.video_element = undefined;
    _this.timeline = undefined;
    _this.camera_data = undefined;

    function init(video_element_id){
        // TODO: init video_element and timeline
        _this.video_element = $("#" + video_element_id);
        _this.timeline = ''; // TODO: CREATE TIMELINE
        _this.camera_data = '';
    }

    function set_camera(camera_data){
        _this.camera_data = camera_data;
    }

}