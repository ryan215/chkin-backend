d3.json('/dashboard/stats/counts', function(data) {
    var testData = [
        {
            key: "User",
            area: true,
            values: data.message.map(function(d) {
                return {'x': d.ts, 'y': d.count}
            })
        }
    ]

    nv.addGraph(function() {
        var chart = nv.models.lineChart()
            .margin({top: 20, bottom: 40, left: 60, right: 60})
            .showLegend(false)
            .color(keyColor);

        chart.yAxis
            .showMaxMin(false)
            .tickFormat(d3.format(',.f'));

        chart.xAxis
            .showMaxMin(false)
            .tickFormat(function(d) { return d3.time.format('%b %d')(new Date(d)) });

        d3.select('#sources-chart-line svg')
            .datum(testData)
            .transition().duration(500)
            .call(chart);
        
        chart.update()

        return chart;
    });
    
    date_format = d3.time.format('%b %d, %Y')

    $row = d3.select("#table-records").selectAll("tr")
        .data(data.message).enter()
        .append("tr")
    
    $row.append("td").text(function(d) { return date_format(new Date(d.ts)) })
    $row.append("td").text(function(d) { return d.count })

    var unsortableColumns = [];
    $('#datatable-table').find('thead th').each(function(){
        if ($(this).hasClass( 'no-sort')){
            unsortableColumns.push({"bSortable": false});
        } else {
            unsortableColumns.push(null);
        }
    });

    $("#datatable-table").dataTable({
        "sDom": "<'row table-top-control'<'col-md-6 hidden-xs per-page-selector'l><'col-md-6'f>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ &nbsp; records per page"
        },
        "iDisplayLength": 50,
        "aaSorting": [[ 0, "desc" ]],
        "aoColumns": unsortableColumns
    });

    $(".dataTables_length select").select2({
        minimumResultsForSearch: 10
    });
})

d3.json('/dashboard/usage/histogram', function(data) {
    var bin_num = data.message.config.bin_num
    var testData = {
        'cost': [
            {
                key: "Cost",
                area: true,
                values: d3.entries(data.message.daily.cost).map(function(d) {
                    return {'x': parseFloat(d.key), 'y': d.value}
                }).sort(function(a,b) { return d3.ascending(a.x, b.x) })
            }
        ],
        'storage': [
            {
                key: "Storage",
                area: true,
                values: d3.entries(data.message.daily.storage).map(function(d) {
                    return {'x': d.key/1024/1024, 'y': d.value}
                }).sort(function(a,b) { return d3.ascending(a.x, b.x) })
            }
        ],
        'downloaded': [
            {
                key: "Downloaded",
                area: true,
                values: d3.entries(data.message.daily.downloaded).map(function(d) {
                    return {'x': d.key/1024/1024, 'y': d.value}
                }).sort(function(a,b) { return d3.ascending(a.x, b.x) })
            }
        ]
    }
    
    console.log(testData)

    color = {
        'cost': '#56bc76',
        'storage': '#618fb0',
        'downloaded': '#e5603b',
    }
    
    var hchart = {}

    nv.addGraph(function() {
        var chart1 = nv.models.discreteBarChart()
            .margin({top: 20, bottom: 105, left: 60, right: 60})
            .color(function(d) {return color['cost']});

        chart1.yAxis
            .showMaxMin(false)
            .tickFormat(d3.format(',.f'));

        chart1.xAxis
            .showMaxMin(false)
            .tickFormat(function(d, i) { return '$' + d3.format(',.2f')(d) + ((i == bin_num) ? '+': '')});

        d3.select('#histogram-cost-chart-line svg')
            .datum(testData['cost'])
            .transition().duration(500)
            .call(chart1);
        
        chart1.update()
        
        hchart['cost'] = chart1

        return chart1;
    });

    nv.addGraph(function() {
        var chart2 = nv.models.discreteBarChart()
            .margin({top: 20, bottom: 105, left: 60, right: 60})
            .color(function(d) {return color['storage']});

        chart2.yAxis
            .showMaxMin(false)
            .tickFormat(d3.format(',.f'));

        chart2.xAxis
            .showMaxMin(false)
            .tickFormat(function(d, i) { return d3.format(',.f')(d) + ((i == bin_num) ? '+': '') + ' MB'});

        d3.select('#histogram-storage-chart-line svg')
            .datum(testData['storage'])
            .transition().duration(500)
            .call(chart2);
        
        chart2.update()

        hchart['storage'] = chart2

        return chart2;
    });

    nv.addGraph(function() {
        var chart3 = nv.models.discreteBarChart()
            .margin({top: 20, bottom: 105, left: 60, right: 60})
            .color(function(d) {return color['downloaded']});

        chart3.yAxis
            .showMaxMin(false)
            .tickFormat(d3.format(',.f'));

        chart3.xAxis
            .showMaxMin(false)
            .tickFormat(function(d, i) { return d3.format(',.f')(d) + ((i == bin_num) ? '+': '') + ' MB'});

        d3.select('#histogram-downloaded-chart-line svg')
            .datum(testData['downloaded'])
            .transition().duration(500)
            .call(chart3);
        
        chart3.update()

        hchart['downloaded'] = chart3
        return chart3;
    });
    
    $(".stats-radio").click(function() {
        shown = $(this).data('id')
        $('.histogram').addClass('hidden')
        $('#histogram-'+shown+'-chart-line').removeClass('hidden')
        hchart[shown].update()
    })

})
