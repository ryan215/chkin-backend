d3.json('/dashboard/usage/records', function(data) {
    var size_format = d3.format(",.2f");
    var cost_format = d3.format(",.4f");
    var date_format = d3.time.format('%b %d, %Y')

    $row = d3.select("#table-records").selectAll("tr")
        .data(data.message).enter()
        .append("tr")
    
    $row.append("td").text(function(d) { return d.type })
    $row.append("td").text(function(d) { return d.name })
    $row.append("td").text(function(d) { return date_format(new Date(d.date)) })
    $row.append("td").text(function(d) { return cost_format(d.cost) })
    $row.append("td").text(function(d) { return size_format(d.storage/1024.0/1024.0) })
    $row.append("td").text(function(d) { return size_format(d.downloaded/1024.0/1024.0) })

    var unsortableColumns = [];
    $('#datatable-table').find('thead th').each(function(){
        if ($(this).hasClass( 'no-sort')){
            unsortableColumns.push({"bSortable": false});
        } else {
            unsortableColumns.push(null);
        }
    });

    $("#datatable-table").dataTable({
        "sDom": "<'row table-top-control'<'col-md-6 hidden-xs per-page-selector'l><'col-md-6'f>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ &nbsp; records per page"
        },
        "iDisplayLength": 50,
        "aaSorting": [[ 0, "desc" ]],
        "aoColumns": unsortableColumns
    });

    $(".dataTables_length select").select2({
        minimumResultsForSearch: 10
    });
})


