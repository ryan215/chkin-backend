from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import View, TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            #return HttpResponse('Hello World!');
            return super(IndexView, self).get(self, *args, **kwargs)
        else:
            return super(IndexView, self).get(self, *args, **kwargs)
