import os
from datetime import datetime
import urllib

from fabric.contrib import django
from fabric.operations import local
from fabric.utils import puts

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
django.settings_module('chkincam.settings')

import django

django.setup()

from django.contrib.auth import get_user_model
from django.core.files import File
from apps.camera.models import VideoSegment

ADMIN_EMAIL_ADDRESS = 'admin@example.com'


def add_admin():
    user_model = get_user_model()
    admin = user_model(email=ADMIN_EMAIL_ADDRESS)
    admin.is_superuser = True
    admin.is_staff = True
    admin.cherrypy_password = 'admin'
    admin.set_password('admin')
    admin.save()


def add_video_segments():
    video_dir = os.path.join(BASE_DIR, 'data')
    vs_filenames = ['demo1.flv', 'demo2.flv']
    vs_thubmnails = ['demo1.png', 'demo2.png']

    video_segments = [
        dict(owner=get_user_model().objects.get(email=ADMIN_EMAIL_ADDRESS),
             utc_start=datetime.now(),
             duration=1.0,
             chkin_version='1',
             video_width='480',
             video_height='240',
             title='demo1',
             description='demo1 description',
             video_name='demo1',
             # thumbnail_blobkey = models.ImageField(blank=True, null=True)
             thumbnail_width='480',
             thumbnail_height='240',
             camera='camera',
             username=ADMIN_EMAIL_ADDRESS,
             user=get_user_model().objects.get(email=ADMIN_EMAIL_ADDRESS),
             size=10),
        dict(owner=get_user_model().objects.get(email=ADMIN_EMAIL_ADDRESS),
             utc_start=datetime.now(),
             duration=1.0,
             chkin_version='1',
             video_width='480',
             video_height='240',
             title='demo2',
             description='demo2 description',
             video_name='demo2',
             thumbnail_width='480',
             thumbnail_height='240',
             camera='camera',
             username=ADMIN_EMAIL_ADDRESS,
             user=get_user_model().objects.get(email=ADMIN_EMAIL_ADDRESS),
             size=10),
    ]

    for i, vs in enumerate(video_segments):
        vso = VideoSegment.objects.create(**vs)
        result_thumbnail = urllib.urlretrieve(os.path.join(video_dir, vs_thubmnails[i]))
        vs_thm_file = File(open(result_thumbnail[0]))
        vso.thumbnail_blobkey.save(vs_thubmnails[i], vs_thm_file, True)

        result_vs = urllib.urlretrieve(os.path.join(video_dir, vs_filenames[i]))
        vs_file = File(open(result_vs[0]))
        vso.video_data_blobkey.save(vs_filenames[i], vs_file, True)


def initial_data():
    puts('==> Hi there! :)')
    puts('==> Installing the requirements...')
    local('pip install -r requirements/dev.txt ')
    puts('==> Creating the db...')
    local('python manage.py migrate')
    local('python manage.py bower install')
    puts('==> Creating the superuser...')
    add_admin()
    puts('==> Adding initial data...')
    add_video_segments()
    puts('==> All done. Please do your thing and leave :)')
