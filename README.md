# README #

### Basic setup ###
#### Python dependencies and database setup
 
 In the terminal (with your python virtualenv activated):
 
```
#!python
pip install -r requirements/dev.txt
fab initial_data

```

Next copy the file *chkincam/settings/local.example.py* and place it in the same directory under name *"local.py"*.

#### Html/css/js dependencies

```
#!bash

npm -g install bower (Node should be installed)
```

```
#!python
python manage.py bower install

```

Celery worker start:

```
#!bash

celery -A chkincam worker -l info
```

Celery events monitor:

```
#!bash

celery -A chkincam events
```

